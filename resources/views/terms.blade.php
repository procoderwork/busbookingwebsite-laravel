@extends('layouts.app')

@section('content')

<div class="container">
  <div class="tmm-multilevel-ol"> 
      <h3>TERMS OF USE</h3>
        <p>(Updated on 08-August, 2016)</p>
        <ol>
            <li>
                <strong>GENERAL</strong>
                <p>
                    <strong>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY. BY ACCESSING THIS WEBSITE AND ANY PAGES THEREOF, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS BELOW. IF YOU DO NOT AGREE TO THE TERMS AND CONDITIONS BELOW, DO NOT ACCESS, VIEW, OR USE THIS WEBSITE, OR ANY PAGES THEREOF.</strong>
                </p>
                <p>
                    <a href="//ticketmeme.com" target="_blank">www.ticketmeme.com</a> (hereinafter referred to as “<strong>Website</strong>”) is owned and operated by Ticketmem.com, a private limited company incorporated under the Indian Companies Act, 1956, having their registered address at1024/82/4, Panathur Main Road, Varthur Hobli, Bangalore - 560103, Karnataka, India (hereinafter referred to as “<strong>Company</strong>”, “<strong>Us</strong>” or “<strong>We</strong>”). These are the Terms of Use under which you (i.e. “User”) who may use the Website.
                </p>
                <p>
                    By using the Website and the available services, you indicate your agreement to these Terms of Use (TOU), including the Privacy Policy (collectively hereinafter “<strong>Agreement</strong>”). The Agreement may change over time and without any advance notification you should also occasionally visit this page to review any modifications to the Agreement because they are binding of you.
                </p>
                <p>
                    If at any time, any new feature or sub-page is added/started by the Company within the Website, or any links to external/related sub-pages are created/developed by the Company then in those cases the specific Agreement and terms for all such pages/links, which are available on such sub-pages shall be applicable to the Users. However, if any inconsistency is found between this Agreement and any other specific agreement then in that case the terms that are most recent would supersede any preceding terms.
                </p>
            </li>
            <li>
                <strong>SERVICE OVERVIEW</strong>
                <p>
                    The Website is an online platform that provides a seamless experience of directing and webcasting events (collectively hereinafter “<strong>Services</strong>”), allowing You to expand Your reach to millions of viewers worldwide in an efficient and interactive manner. The Website enables Users to submit videos to Us from any device and deliver them either Live or as Video on Demand (VOD) and We will upload on Our platform. In order to give You an undisturbed, high-quality streaming experience, the Website has created its own encoding platform to receive your ingested video and redeliver it to the world using one of the number one ranked Content Delivery Network.
                </p>
                <p>
                    The Services provided by the Website include:
                    <ol>
                        <li>Video-Streaming Services (both Live and Recorded)</li>
                        <li>Sale of Hardware Encoders </li>
                        <li>Sale of monthly/yearly subscriptions</li>
                    </ol>
                    <p>
                        These Services will be replete with the following features offered by the Website:
                    </p>
                    <ol>
                      <li>Single/Multi-camera Coverage</li>
                      <li>Private Logo Branding</li>
                      <li>Play on any Device</li>
                      <li>Bitrate adaptive streaming</li>
                      <li>Embed on any website</li>
                      <li>Add sub-videos, photos and texts</li>
                      <li>Social media connections to grow Your followers</li>
                      <li>Internet connection bonding</li>
                      <li>Full HD input/output</li>
                      <li>World’s best content delivery network</li>
                    </ol>
                </p>
                <p>
                    User(s) understand and accept that it is the sole responsibility of the User(s) to submit the videos and voice content to Us and the Company shall, under no circumstances, provide any form of content. 
                </p>
                <p>
                    The Website has no obligation to screen or censor any of the Website’s content, nor is the Company responsible for any virtual or actual transactions between Users. You further agree and understand that we do not endorse, market or promote any of the postings, products, services, information or entities, nor do we at any point in time come into possession of or engage in the distribution of any of the goods, services or entities about which you have submitted to Us or provided information about on our Website. In the event that You interact with other Users on our Website with respect to any posting or information, we strongly encourage you to exercise reasonable diligence as you would in traditional off line channels and practice judgment and common sense before committing to sharing or exchanging information.
                </p>
                <p>
                    While making use of Our Services and other ancillary features such as the discussion forums, comments and feedback (if any), you will post in the appropriate category or area and you agree that your use of the Website shall be strictly governed by this Agreement which shall not violate the prohibited and restricted items policy:
                </p>
                <ol>
                    <li>
                        <p>
                            "Your Information" is defined as any information You provide to Us or other User(s) of the Website during registration), sharing videos for streaming, subscribing, engaging in purchase of the Company’s products, in the feedback area, through the discussion forums or in the course of using any other feature of the Services. You agree that you are the lawful owner having all rights, title and interest in your information, and further agree that you are solely responsible and accountable for Your Information and that we act as a mere platform for your online distribution and publication of Your Information.
                        </p>
                        <p>
                            User(s) understand and accept that information about them, including but not limited to browsing behaviour and pages viewed will be collected through data collections like cookies by the Website and will be used to ascertain user behaviour and to gather broad demographic information.
                        </p>
                    </li>
                </ol>
            </li>
            <li>
                <strong>REGISTRATION</strong>
                <p>
                    It is optional for the User to register in the Website. Basic information like Name, Email ID, Username, Password, Contact Number and Location (City) would be collected from the User. The Email ID provided during registration can be of any Email service provider including but not limited to Gmail, and Yahoo mail. You also have the option of sharing videos in your social media accounts, such as Your Facebook or Google Plus account. You must keep your account and registration details current and correct.
                </p>
                <p>
                    By registering on the Website, you understand and accept that you are responsible for maintaining the confidentiality of your User ID, Password and Email Address and for restricting access to your computer, computer system, computer network and your account on the Website, and you are responsible for all activities that occur under your User ID and password.
                </p>
                <p>
                    By registering, you certify that all information you provide, now or in the future, is accurate. The Website reserves the right, in its sole discretion, to deny you access to this website or any portion thereof without notice,
                    <ol class="ck-small-latin">
                        <li>
                            For any unauthorised access or use by you;
                        </li>
                        <li>
                            If you assign or transfer (or attempt the same) any rights granted to you under this Agreement; 
                        </li>
                        <li>
                            If you violate any of the other terms and conditions of this Agreement.
                        </li>
                    </ol>
                </p>
            </li>
            <li>
                <strong>ELIGIBILITY</strong>
                <p>
                    The Services are not available to persons under the age of 18 or to anyone previously suspended or removed from the services provided by the Website. By accepting these Terms & Conditions or by otherwise using the Services, You represent that You are at least 18 years of age and have not been previously suspended or removed from the Services. You represent and warrant that you have the right, authority, and capacity to enter into this Agreement and to abide by all of the terms and conditions of this Agreement. You shall not impersonate any person or entity, or falsely state or otherwise misrepresent identity, age or affiliation with any person or entity.
                </p>
                <p>
                    In case You are less than 18 years of age, You shall use the Services available on the Website under the supervision of Your legal guardian.
                </p>
            </li>
            <li>
                <strong>OBLIGATIONS OF THE USER</strong>
                <p>
                    You represent that you have the legal authority to access this Website, and at all times, will provide true, accurate and complete information when submitting any information, including, without limitation, when you provide information during registration or other forms as may be applicable. If you provide any false, inaccurate, or incomplete information, we reserve the right to terminate your access immediately to the Website. Without limiting the generality of the foregoing, you agree that you shall not use nor disclose to any other party in a manner not permitted by this Agreement any personally identifiable information, which you receive or which is made available from us in connection with this Agreement. In addition, you also acknowledge and agree that use of the Internet and access to or transmissions or communications with the Website is solely at your own risk. While we have endeavoured to create a secure and reliable Website, you should understand that the confidentiality of any communication or material transmitted to or from the Website over the Internet or other form of communication network cannot be guaranteed and the Website is not responsible for the security of any information transmitted to or from the Website. You agree to assume all responsibility concerning activities related to your use of the Website.
                </p>
            </li>
            <li>
                <strong>PAYMENT/CANCELLATION/REFUND TERMS</strong>
                <p>
                    All the payment related obligations and facilities on the Website are undertaken on a third party payment gateway including but not limited to PayUmoney. The Company is not the relevant party responsible or operating the payment transactions.
                </p>
                <p>
                    While making payment using the online third party payment gateway platform/system, you agree and acknowledge that the Company is not responsible or shall not assume any liability, whatsoever in respect of any loss or damage arising directly or indirectly to you due to reasons not limited to -:
                    <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Misuse or illegal duplication of the authorization for any transaction/s; and<p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Impersonation by any third party/individual in relation to use of debit/credit card; and</p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Any issues/ error which may arise at the end of your Bank; and</p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Any payment issues arising due to decline of transaction for any other reason/s whatsoever.</p>
                </p>
                
                </p>
                <p>
                    All instruments processed by the payment gateway or appropriate payment system infrastructure will also be governed by the terms and conditions agreed with the respective Issuing Bank/institutions in addition to the present terms and conditions.
                </p>
                <p>
                    User acknowledges that the Website shall not be held liable for any damages, interests or claims etc. resulting from not processing the payment transaction or any delay in processing the payment transaction which is beyond the control of the Company.
                </p>
                <p>
                    <strong>Supported modes for online payment:  Yes, all 3 options are correct</strong>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Credit Cards issued in India – MasterCard, Visa, American Express</p>
                  <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Debit Card issued in India – MasterCard, Maestro, Visa</p>
                  <p><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Net Banking / Online Transfer. </p>
                  <p>
                      User(s) understand and accept that the payment will comprise of the cost of the service(s) being provided and the applicable central and state government taxes. 
                  </p>
                  <p>
                      User(s) understand and accept the prices of the services being provided along with the offers on these services are subject to variation.
                  </p>
                  <p>
                      User(s) understand and accept that payment once made CANNOT BE CANCELLED OR REFUNDED.  
                  </p>
                </p>
            </li>
            <li>
                <strong>VIDEO MONETISATION  </strong>
                <p>
                    The Website allows Monetization of videos which includes but limited to authorisation by the User to the presence of advertisements on Your videos, and the Website charges advertisers for these and shares the fees with You, allowing You to make some money from people viewing Your videos. You must meet these minimum requirements in order to monetize your videos:
                    <ol class="ck-small-latin">
                        <li>
                            Your content is advertiser-friendly
                        </li>
                        <li>
                            You created the content or have permission to use it commercially. 
                        </li>
                        <li>
                            You are able to provide documentation proving you own commercial rights to all audio and video content.
                        </li>
                        <li>
                            Your content complies with the Website’s Terms of Service. The Company reserves the right to disable monetization for accounts that do not follow our guidelines.
                        </li>
                    </ol>
                </p>
            </li>
            <li>
                <strong>PROHIBITED USES OF OUR WEBSITE</strong>
                <p>
                    Users warrant to only utilize the Services of the Website in accordance with these Terms of Use and any relevant statutory provisions. Users are prohibited from violating the security of any Website by engaging in the following: 
                    <ol class="ck-small-latin">
                        <li>
                            accessing data not intended for such user or logging into an account which the user is not authorized to access;
                        </li>
                        <li>
                            attempting to test the vulnerability of a system or to breach any security or authentication measures; or 
                        </li>
                        <li>
                            attempting to overload, spam, or crash any Website by means of a “virus” or other software “attack” upon the Website’s integrity. Any attempted Website violations may result in civil or criminal liability, and the Company will prosecute users who are involved in such violations.
                        </li>
                    </ol>
                </p>
                <p>
                    Users may not use any Website in order to transmit, or store material 
                    <ol class="ck-small-latin">
                        <li>
                            in violation of any applicable law or regulation, 
                        </li>
                        <li>
                            in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy, publicity or other personal rights of others, 
                        </li>
                        <li>
                            that is defamatory, obscene, threatening, abusive or hateful, or 
                        </li>
                        <li>
                            in a manner which jeopardizes India’s national security, contributes to social unrest, or sectarian violence.
                        </li>
                    </ol>
                </p>
                <p>
                    All users agree not to engage in any activity which seeks to take advantage of or exploit other users of the Website or of the Website themselves. This prohibited behaviour includes, but is not limited to, the following: 
                    <ol class="ck-small-latin">
                      <li>
                          Copying /Downloading the video and if provided access to download, not distributing the video across any other medium
                      </li>
                      <li>
                          Copying Our video URL and embedding it in Your website
                      </li>
                      <li>
                          Using Our video URL for any unauthorized purpose or integrating Our video URL integrate in third party websites  
                      </li>
                      <li>
                          Reproducing, making available online or electronically transmitting, publishing, adapting, distributing, transmitting, broadcasting, displaying, selling, licensing, or otherwise exploiting any Content for any other purposes without the prior written consent of the Website or the respective licensors of the Content. 
                      </li>
                    </ol>
                </p>
            </li>
            <li>
                <strong>LINKS TO THIRD PARTY SITES</strong>
                <p>
                    The links in this site may allow you to leave the Website. The linked sites are not under the control of the Website. The Website has not reviewed, nor approved these sites and is not responsible for the contents or omissions of any linked site or any links contained in a linked site. The inclusion of any linked site does not imply endorsement by the Website of the site. Third party links to the Website shall be governed by a separate agreement.
                </p>
            </li>
            <li>
                <strong>CONTENTS POSTED ON SITE</strong>
                <p>
                    All text, graphics, user interfaces, visual interfaces, photographs, trademarks, logos, sounds, music and artwork (collectively, "Content"), is a third party user generated content and the Website has no control over such third party user generated content as the Website is merely an intermediary for the purposes of this Terms of Use. You understand and accept that the Company has the full right to delete any unauthorised data anytime without permission of the User.
                </p>
                <p>
                    Except as expressly provided in these Terms of Use, no part of the Website and no Content may be copied, reproduced, republished, uploaded, posted, publicly displayed, encoded, translated, transmitted or distributed in any way (including "mirroring") to any other computer, server, Website or other medium for publication or distribution or for any commercial enterprise, without express prior written consent from the Website.
                </p>
                <p>
                    You may use information on the products and services purposely made available on the Website for downloading, provided that You (1) do not remove any proprietary notice language in all copies of such documents, (2) use such information only for your personal, non-commercial informational purpose and do not copy or post such information on any networked computer or broadcast it in any media, (3) make no modifications to any such information, and (4) do not make any additional representations or warranties relating to such documents.
                </p>
                <p>
                    You shall be responsible for any notes, messages, emails, billboard postings, photos, drawings, profiles, opinions, ideas, images, videos, audio files or other materials or information posted or transmitted to the Website (collectively, "Content"). Such Content will become Our property and You grant Us the worldwide, perpetual and transferable rights in such Content. We shall be entitled to, consistent with Our Privacy Policy as adopted in accordance with applicable law, use the Content or any of its elements for any type of use forever, including but not limited to promotional and advertising purposes and in any media whether now known or hereafter devised, including the creation of derivative works that may include the Content You provide. You agree that any Content You post may be used by us, consistent with Our Privacy Policy and Rules of Conduct on Website as mentioned herein, and You are not entitled to any payment or other compensation for such use.
                </p>
            </li>
            <li>
                <strong>COMMUNICATION POLICY</strong>
                <p>
                    By accepting the terms and conditions the User accepts that the Website may send the alerts to the mobile phone number/e-mail address provided by the User while registering for the service or to any such number/e-mail replaced and informed by the User. The User acknowledges that the alerts will be received only if the mobile phone is in 'On' mode to receive the SMS. If the mobile phone is in 'Off' mode then the User may not get / get after delay any alerts sent during such period. The User acknowledges that the SMS service/e-mail service provided by the Website is an additional facility provided for the User's convenience and that it may be susceptible to error, omission and/ or inaccuracy. In the event the User observes any error in the information provided in the alert, the Website shall be immediately informed about the same by the User and the Website will make best possible efforts to rectify the error as early as possible. The User shall not hold the Website liable for any loss, damages, claim, expense including legal cost that may be incurred/ suffered by the User on account of the SMS/e-mail facility. The User acknowledges that the clarity, readability, accuracy, and promptness of providing the service depend on many factors including the infrastructure, connectivity of the service provider. The Website shall not be responsible for any non-delivery, delayed delivery or distortion of the alert in any way whatsoever. By accepting the terms and conditions the User acknowledges and agrees that the Website may call the mobile phone number provided by the User while registering for the service or to any such number replaced and informed by the User, for the purpose of collecting feedback from the User regarding their usage of services.
                </p>
            </li>
            <li>
                <strong>COPYRIGHT POLICY</strong>
                <p>
                    The Website reserves the right to terminate User access to the Website if a User has been found to be a “repeat infringer” of the copyrights of any third party. A “repeat infringer” is a User who has been notified of indulging in infringing activity more than 3 times.
                </p>
            </li>
            <li>
                <strong>WARRANTY DISCLAIMER</strong>
                <p>
                    YOU AGREE THAT YOUR USE OF THE SERVICES SHALL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, ticketmeme, ITS OFFICERS, DIRECTORS, EMPLOYEES, AND AGENTS EXCLUDE ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SERVICES AND YOUR USE THEREOF. TO THE FULLEST EXTENT PERMITTED BY LAW, ticketmeme EXCLUDES ALL WARRANTIES, CONDITIONS, TERMS OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SITE'S CONTENT OR THE CONTENT OF ANY SITES LINKED TO THIS SITE AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICES. ticketmeme DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICES OR ANY HYPERLINKED SERVICES OR FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND ticketmeme WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.
                </p>
            </li>
            <li>
                <strong>TERMINATION</strong>
                <p>
                    This Agreement and the license rights granted hereunder shall remain in full force and effect unless terminated or cancelled for any of the following reasons: 
                    <ol class="ck-small-latin">
                      <li>
                          For any unauthorized access or use by you;
                      </li>
                      <li>
                          If you assign or transfer (or attempt the same) any rights granted to you under this Agreement; 
                      </li>
                      <li>
                          If you violate any of the other terms and conditions of this Agreement.
                      </li>
                    </ol>
                </p>
                <p>
                    Termination or cancellation of this Agreement shall not affect any right or relief to which the Website may be entitled, at law or in equity. Upon termination of this Agreement, all rights granted to you will terminate and revert to the Website. Except as set forth herein, regardless of the reason for cancellation or termination of this Agreement, the fee charged if any for access to the Website is non-refundable for any reason.
                </p>
            </li>
            <li>
                <strong>JURISDICTION </strong>
                <p>
                    The terms of this Agreement are exclusively based on and subject to Indian law. You hereby consent to the exclusive jurisdiction and venue of courts in Bangalore, Karnataka, India in all disputes arising out of or relating to the use of this website. Use of this website is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph.
                </p>
            </li>
            <li>
                <strong>INDEMNIFICATION</strong>
                <p>
                    YOU SHALL INDEMNIFY, DEFEND AND HOLD HARMLESS THE  WEBSITE (INCLUDING ITS AND THEIR OFFICERS, DIRECTORS, EMPLOYEES, AFFLIATES, GROUP COMPANIES, AGENTS, REPRESENTATIVES OR SUBCONTRACTORS) FROM ANY AND ALL CLAIMS AND LOSSES IMPOSED ON, INCURRED BY OR ASSERTED AS A RESULT OF OR RELATED TO: 
                    <ol class="ck-small-latin">
                      <li>
                          YOUR ACCESS AND USE OF THE WEBSITE
                      </li>
                      <li>
                          ANY NON-COMPLIANCE BY USER WITH THE TERMS AND  CONDITIONS HEREOF; OR 
                      </li>
                      <li>
                          ANY THIRD PARTY ACTIONS RELATED TO USERS RECEIPT AND USE OF THE INFORMATION, WHETHER AUTHORIZED OR UNAUTHORIZED.
                      </li>
                    </ol>
                </p>
            </li>
            <li>
                <strong>ASSIGNMENT</strong>
                <p>
                    No User (s) may transfer or assign any rights or licences granted by the Agreement. However, the Website reserves the right to assign or transfer these rights and licences without restriction. 
                </p>
            </li>
            <li>
                <strong>MISCELLANEOUS PROVISIONS</strong>
                <p>
                    <ol class="ck-small-latin">
                        <li>
                            <strong><i>Entire Agreement: </i></strong> This Agreement is the complete and exclusive statement of the agreements between you and us with respect to the subject matter hereof and supersedes all other communications or representations or agreements (whether oral, written or otherwise) relating thereto.
                        </li>
                        <li>
                            <strong><i>Waiver: </i></strong>The failure of either party at any time to require performance of any provision of this Agreement in no manner shall affect such party's right at a later time to enforce the same. No waiver by either party of any breach of this Agreement, whether by conduct or otherwise, in any one or more instances, shall be deemed to be, or construed as, a further or continuing waiver of any other such breach, or a waiver of any other breach of this Agreement.
                        </li>
                        <li>
                            <strong><i>Severability: </i></strong>If any provision of this Agreement shall to any extent be held invalid, illegal or unenforceable, the validity, legality and enforceability of the remaining provisions of this Agreement shall in no way be affected or impaired thereby and each such provision of this Agreement shall be valid and enforceable to the fullest extent permitted by law. In such a case, this Agreement shall be reformed to the minimum extent necessary to correct any invalidity, illegality or unenforceability, while preserving to the maximum extent the rights and commercial expectations of the parties hereto, as expressed herein. 
                        </li>

                    </ol>
                </p>
            </li>
            <li>
                <strong>CONTACT US</strong>
                <p>
                    If you have any questions about this Agreement, the practices of the Company, or your experience with the Service, please send an email to info@ticketmeme.com.
                </p>
            </li>

        </ol>
  </div>

</div>
    
    
@endsection
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="ticketmeme">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Bootstrap, Parallax, Template, Registration, Landing">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="author" content="Grayrids">
  <title>TicketMeMe::One Search For All Your Travel Needs With Best Discount</title>
  <base href="{{URL::asset('/')}}" target="_top">

  <link rel="stylesheet" href="{{{ URL::asset('css/font-awesome.min.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/line-icons.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/owl.carousel.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/owl.theme.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/nivo-lightbox.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/magnific-popup.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/slicknav.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/animate.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/main.css')}}}">

  <link rel="stylesheet" href="{{{ URL::asset('css/font-awesome.min.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/line-icons.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/responsive.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('icon-fonts/icon-fonts-basic.css')}}}" type="text/css" media="screen" />
  <link rel="stylesheet" href="{{{ URL::asset('icon-fonts/icon-font-arrow.css')}}}" type="text/css" media="screen" />
  <link rel="stylesheet" href="{{{ URL::asset('icon-fonts/tmm-icons.css')}}}" type="text/css" media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" type="text/css" media="screen" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/css/tempusdominus-bootstrap-4.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
  <link rel="stylesheet" href="{{{ URL::asset('css/second.css')}}}">
  <style type="text/css">
  .autocomplete-suggestions { border: 1px solid #999; background: #F0F0F0; overflow: auto; }
  .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; font-size: 14px }
  .autocomplete-selected { background: #47730D;color: white}
  .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
  .autocomplete-group { padding: 2px 5px; }
  .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
  </style>
</head>

<body class="no-skin">
    @include('partials.header')
    <main autoscroll="true">
        @yield('content')
    </main>
    @include('partials.footer')
    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.4.7/jquery.autocomplete.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="{{{ URL::asset('js/owl.carousel.js')}}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="{{{ URL::asset('js/slick.js')}}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    {{--  <script src="{{{ URL::asset('js/date-picker/datepicker-directive.js')}}}"></script>
    <script src="{{{ URL::asset('js/app.js')}}}"></script>  --}}
    <script src="{{{ URL::asset('js/main.js')}}}"></script>

    @yield('footer_script')
</body>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Admin - TicketMeMe</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/main.css')}}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Additional CSS -->
    @yield('additional_css')
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="{{url('/')}}">TicketMeMe</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="{{route('admin.home')}}"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="{{route('admin.logout')}}"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">{{session('admin_username')}}</p>
          <p class="app-sidebar__user-designation">{{session('admin_email')}}</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.home' ? 'active' : '')}}" href="{{route('admin.home')}}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Settings</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.ticket.index' || \Request::route()->getName()=='admin.ticket.edit' ? 'active' : '')}}" href="{{route('admin.ticket.index')}}"><i class="app-menu__icon fa fa-ticket"></i><span class="app-menu__label">Manage Ticket Posting</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.user.index' ? 'active' : '')}}" href="{{route('admin.user.index')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">User Management</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.feedback.index' || \Request::route()->getName()=='admin.feedback.show' ? 'active' : '')}}" href="{{route('admin.feedback.index')}}"><i class="app-menu__icon fa fa-comments"></i><span class="app-menu__label">Feedbacks</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.faq.index' || \Request::route()->getName()=='admin.faq.add'|| \Request::route()->getName()=='admin.faq.edit' ? 'active' : '')}}" href="{{route('admin.faq.index')}}"><i class="app-menu__icon fa fa-question"></i><span class="app-menu__label">Faqs</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.subscribe.index' ? 'active' : '')}}" href="{{route('admin.subscribe.index')}}"><i class="app-menu__icon fa fa-thumbs-up"></i><span class="app-menu__label">Subscribe</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.bustype.index'|| \Request::route()->getName()=='admin.bustype.edit' ? 'active' : '')}}" href="{{route('admin.bustype.index')}}"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">Bus Types</span></a></li>
        <li><a class="app-menu__item {{(\Request::route()->getName()=='admin.busoperator.index'|| \Request::route()->getName()=='admin.busoperator.edit' ? 'active' : '')}}" href="{{route('admin.busoperator.index')}}"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">Bus Operators</span></a></li>
      </ul>
    </aside>
    <main class="app-content">
      @yield('content')
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="{{asset("admin_assets/js/jquery-3.2.1.min.js")}}"></script>
    <script src="{{asset("admin_assets/js/popper.min.js")}}"></script>
    <script src="{{asset("admin_assets/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("admin_assets/js/main.js")}}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{asset("admin_assets/js/plugins/pace.min.js")}}"></script>
    <!-- Additional scripts -->
    @yield('footer_scripts')
  </body>
</html>

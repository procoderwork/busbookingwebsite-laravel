<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="ticketmeme">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Bootstrap, Parallax, Template, Registration, Landing">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="author" content="Grayrids">
  <title>TicketMeMe::One Search For All Your Travel Needs With Best Discount</title>
  <base href="{{URL::asset('/')}}" target="_top">
  <!-- Bootstrap CSS -->
  <!-- <link rel="apple-touch-icon" sizes="57x57" href="{{{ URL::asset('img/favicon/apple-icon-57x57.png')}}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{{ URL::asset('img/favicon/apple-icon-60x60.png')}}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{{ URL::asset('img/favicon/apple-icon-72x72.png')}}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{{ URL::asset('img/favicon/apple-icon-76x76.png')}}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{{ URL::asset('img/favicon/apple-icon-114x114.png')}}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{{ URL::asset('img/favicon/apple-icon-120x120.png')}}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{{ URL::asset('img/favicon/apple-icon-144x144.png')}}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{{ URL::asset('img/favicon/apple-icon-152x152.png')}}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{{ URL::asset('img/favicon/apple-icon-180x180.png')}}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{{ URL::asset('img/favicon/android-icon-192x192.png')}}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{{ URL::asset('img/favicon/favicon-32x32.png')}}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{{ URL::asset('img/favicon/favicon-96x96.png')}}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{{ URL::asset('img/favicon/favicon-16x16.png')}}}">
  <link rel="manifest" href="{{{ URL::asset('img/favicon/manifest.json')}}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{{ URL::asset('img/favicon/ms-icon-144x144.png')}}}">
  <meta name="theme-color" content="#ffffff"> -->

  <link rel="stylesheet" href="{{{ URL::asset('css/font-awesome.min.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/line-icons.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/owl.carousel.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/owl.theme.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/nivo-lightbox.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/magnific-popup.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/slicknav.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/animate.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/main.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/font-awesome.min.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/line-icons.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('css/responsive.css')}}}">
  <link rel="stylesheet" href="{{{ URL::asset('icon-fonts/icon-fonts-basic.css')}}}" type="text/css" media="screen" />
  <link rel="stylesheet" href="{{{ URL::asset('icon-fonts/icon-font-arrow.css')}}}" type="text/css" media="screen" />
  <link rel="stylesheet" href="{{{ URL::asset('icon-fonts/tmm-icons.css')}}}" type="text/css" media="screen" />
  <link rel="stylesheet" href="{{{ URL::asset('css/datepicker.css')}}}" type="text/css" media="screen" />
  <script src="{{{ URL::asset('js/jquery-min.js')}}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.js"></script>
</head>

<body class="no-skin">
    <main autoscroll="true">
        @yield('content')
    </main>

    <script src="{{{ URL::asset('js/popper.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/bootstrap.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.mixitup.js')}}}"></script>
    <script src="{{{ URL::asset('js/nivo-lightbox.js')}}}"></script>
    <script src="{{{ URL::asset('js/owl.carousel.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.stellar.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.nav.js')}}}"></script>
    <script src="{{{ URL::asset('js/scrolling-nav.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.easing.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/smoothscroll.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.slicknav.js')}}}"></script>
    <script src="{{{ URL::asset('js/wow.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.vide.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.counterup.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/jquery.magnific-popup.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/waypoints.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/form-validator.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/slick.js')}}}"></script>
    <script src="{{{ URL::asset('js/date-picker/datepicker-directive.js')}}}"></script>
    <script src="{{{ URL::asset('js/app.js')}}}"></script>
    <script src="{{{ URL::asset('js/main.js')}}}"></script>
</body>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="dashboard-wrapper">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        <h4><i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i></h4> 
                        <span class="nav-label">My Profile</span>
                    </a>
                    <a href="#" class="list-group-item">
                        <h4><i class="fa fa-list-alt fa-2x" aria-hidden="true"></i></h4>
                        <span class="nav-label">Ticket History</span>
                    </a>
                    <a href="#" class="list-group-item">
                        <h4><i class="fa fa-key fa-2x" aria-hidden="true"></i></h4>
                        <span class="nav-label">Change Password</span>
                    </a>
                    <a href="#" class="list-group-item">
                        <h4><i class="fa fa-google-wallet fa-2x" aria-hidden="true"></i></h4>
                        <span class="nav-label">My Wallet</span>
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <form method="POST" action="{{route('post_update_profile')}}">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label for="Name" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ Auth::user()->name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                            <div class="col-sm-9">
                            <input type="text" name="mobile" class="form-control" id="phone" placeholder="phone" value="{{ Auth::user()->mobile }}"
                            >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email Address</label>
                            <div class="col-sm-9">
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email Address" value="{{ Auth::user()->email }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 tmm-centered-btn">
                                <button class="btn btn-primary">Update Profile</button>
                                <button class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">
                    <div class="col-xs-12 u-p-right-small">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <span  class="nav-link active" data-toggle="tab" href="#posted-tickets" role="tab">Posted Tickets</span >
                            </li>
                            <li class="nav-item">
                                <span class="nav-link" data-toggle="tab" href="#requested-tickets" role="tab">Requested Tickets</span >
                            </li>
                        </ul>
                        
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="posted-tickets" role="tabpanel">
                                <ul class="list-group">
                                    @foreach(Auth::user()->tickets as $ticket)
                                    <li class="list-group-item">
                                        <div class="row u-w-100p">
                                            <div class="col-xs-12 col-md-3">
                                                {{date('jS M,Y',strtotime($ticket->created_at))}}
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <span>{{$ticket->source->name}}</span>
                                                <span class="u-p-l-r-small"><i class="icon icon-arrows-slim-right"></i></span>
                                                <span>{{$ticket->destination->name}}</span>
                                            </div>
                                            <div class="col-xs-12 col-md-3">
                                                <span>{{$ticket->bus_operator->name}}</span>
                                            </div>
                                            <div class="col-xs-12 col-md-2">
                                            <button class="btn btn-outline-primary" data-toggle="collapse" data-target="#collapseTicketdetails-{{$ticket->id}}" aria-expanded="false" aria-controls="collapseTicketdetails-{{$ticket->id}}">More Details</button>
                                            </div>
                                        </div>
                                        <div class="collapse collapse-details" id="collapseTicketdetails-{{$ticket->id}}">
                                            <div class="row u-w-100p">
                                                <div class="col-xs-12 col-md-4">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Bus</span>
                                                    <div class="search-details">
                                                    <p><span>{{$ticket->bus_operator->name}}</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Boarding</span>
                                                    <div class="search-details">
                                                    <p>
                                                        <span>{{date('h:i A', strtotime($ticket->journey_time))}}</span><br>
                                                        {{--  <span>Anandrao Junction</span>  --}}
                                                    </p>
                                                    </div>
                                                </div>
                                                {{--  <div class="col-xs-12 col-md-3">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Seat</span>
                                                    <div class="search-details">
                                                    <p>
                                                        <span>Lower Bearth</span><br>
                                                        <span class="badge badge-danger">1</span> Ticket
                                                    </p>
                                                    </div>
                                                </div>  --}}
                                                <div class="col-xs-12 col-md-4">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Fare</span>
                                                    <div class="search-details">
                                                    <p>
                                                        <span>INR {{$ticket->price}}.00</span><br>
                                                        {{--  <span class="badge badge-danger">Actual: INR 650.00</span>  --}}
                                                    </p>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            {{--  <hr>
                                            <div class="row u-w-100p">
                                                <div class="col-xs-12 col-md-6">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Name</span>
                                                    <div class="search-details">
                                                    <p><span>Dadapeer Shekh</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Contact Number</span>
                                                    <div class="search-details">
                                                    <p><span>+91-54785 22150</span></p>
                                                    </div>
                                                </div>
                                            </div>  --}}
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="tab-pane" id="requested-tickets" role="tabpanel">
                                <ul class="list-group">
                                    @foreach(Auth::user()->request_tickets as $request_ticket)
                                    @php
                                    $ticket = $request_ticket->ticket;
                                    @endphp
                                    <li class="list-group-item">
                                        <div class="row u-w-100p">
                                            <div class="col-xs-12 col-md-3">
                                                {{date('jS M,Y',strtotime($ticket->created_at))}}
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <span>{{$ticket->source->name}}</span>
                                                <span class="u-p-l-r-small"><i class="icon icon-arrows-slim-right"></i></span>
                                                <span>{{$ticket->destination->name}}</span>
                                            </div>
                                            <div class="col-xs-12 col-md-3">
                                                <span>{{$ticket->bus_operator->name}}</span>
                                            </div>
                                            <div class="col-xs-12 col-md-2">
                                            <button class="btn btn-outline-primary" data-toggle="collapse" data-target="#collapserequestTicketdetails-{{$ticket->id}}" aria-expanded="false" aria-controls="collapserequestTicketdetails-{{$ticket->id}}">More Details</button>
                                            </div>
                                        </div>
                                        <div class="collapse collapse-details" id="collapserequestTicketdetails-{{$ticket->id}}">
                                            <div class="row u-w-100p">
                                                <div class="col-xs-12 col-md-4">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Bus</span>
                                                    <div class="search-details">
                                                    <p><span>{{$ticket->bus_operator->name}}</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Boarding</span>
                                                    <div class="search-details">
                                                    <p>
                                                        <span>{{date('h:i A', strtotime($ticket->journey_time))}}</span><br>
                                                        {{--  <span>Anandrao Junction</span>  --}}
                                                    </p>
                                                    </div>
                                                </div>
                                                {{--  <div class="col-xs-12 col-md-3">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Seat</span>
                                                    <div class="search-details">
                                                    <p>
                                                        <span>Lower Bearth</span><br>
                                                        <span class="badge badge-danger">1</span> Ticket
                                                    </p>
                                                    </div>
                                                </div>  --}}
                                                <div class="col-xs-12 col-md-4">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Fare</span>
                                                    <div class="search-details">
                                                    <p>
                                                        <span>INR {{$ticket->price}}.00</span><br>
                                                        {{--  <span class="badge badge-danger">Actual: INR 650.00</span>  --}}
                                                    </p>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            {{--  <hr>
                                            <div class="row u-w-100p">
                                                <div class="col-xs-12 col-md-6">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Name</span>
                                                    <div class="search-details">
                                                    <p><span>Dadapeer Shekh</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <span class="badge badge-pill badge-info badge-custom--mini">Contact Number</span>
                                                    <div class="search-details">
                                                    <p><span>+91-54785 22150</span></p>
                                                    </div>
                                                </div>
                                            </div>  --}}
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
    
                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <form action="{{route('post_update_password')}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label for="old-password" class="col-sm-3 col-form-label">Old Password</label>
                            <div class="col-sm-9">
                            <input type="password" class="form-control" id="old-password" placeholder="Old Password" value="{{ Auth::user()->password }}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new-password" class="col-sm-3 col-form-label">New Password</label>
                            <div class="col-sm-9">
                            <input type="password" name="password" class="form-control" id="new-password" placeholder="New Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new-confirm-password" class="col-sm-3 col-form-label">Confirm Password</label>
                            <div class="col-sm-9">
                            <input type="password" name="password_confirmation" class="form-control" id="new-confirm-password" placeholder="Confirm New Password">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 tmm-centered-btn">
                                <button class="btn btn-primary">Update Password</button>
                                <button class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                        <h1 style="font-size:6em;color:#2998ff"><i class="fa fa-info-circle" aria-hidden="true"></i></h1>
                        <h1 style="margin-top: 0;color:#ff7730">Cooming Soon</h1>
                        <h3 style="margin-top: 0;color:#303842">all Leading wallet for easy transaction</h3>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_script')
    <script>
        (function(){
            @if(Session::has('message'))
                @if(Session::get('message')=='resetMobile')
                    swal("Welcome!", "You have newly registered. Please change mobile number.", "success");
                @endif
            @endif
        })();
    </script>
@endsection

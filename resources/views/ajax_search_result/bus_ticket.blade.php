    @php
    $i = 0;
    if(!empty($buses['apiAvailableBuses'])){
    @endphp
    @foreach($buses['apiAvailableBuses'] as $bus)
    @php
    $i++;
    @endphp
    <div>
        <div class="search-wrapper tm-bus-custom-padding">
            <div class="container u-p-relative">
                <div class="row bus-ticket-max-height">
                    <div class="col-xs-12 col-md-3 u-p-relative">
                        <div class="search-details u-p-l-large">
                        <p><strong>{{$bus['operatorName']}}</strong></p>
                            <p>{{$bus['busType']}}</p>
                            <span class="result-bus-icon icontmm-bus"><i class="fa fa-map-marker"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="search-details u-p-l-large">
                            <p>
                                <strong>
                                <span>{{$bus['departureTime']}}</span>
                                    <span class="u-p-l-r-small"><i class="icon icon-arrows-slim-right"></i></span>
                                <span>{{$bus['arrivalTime']}}</span>
                                </strong>
                            </p>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-2">
                        <div class="search-details hide-mobile">
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-2">
                        <div class="search-details u-p-l-large">
                            <p class="u-p-relative">
                                <strong>
                                    <span>{{$bus['availableSeats']}} Seats</span>
                                </strong>
                                <span class="tm-bus-seat-icon"><i class="icontmm-seat"></i></span>
                            </p>
                            <p>
                            {{--  <span>11 Window</span>  --}}
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <div class="search-details u-p-l-large u-text-right search-details-fare">
                        <div>
                        <p><strong class="u-f-medium-bold">INR {{$bus['fare']}}</strong></p>
                        </div>
                        <button class="btn btn-primary u-m-l-small btn-tm-search" onclick="
                        @if(!Auth::check())
                        document.location.href = '{{url('/login')}}';"
                        @else
                        getSeatLayout('{{$source}}','{{$destination}}','{{$jdate}}','{{$bus['inventoryType']}}','{{$bus['routeScheduleId']}}')

                        " data-toggle="collapse" data-target="#collapseViewSeats-{{$i}}"
                        @endif >
                            <span>View Seats</span>
                        </button>
                        </div>
                    </div>
                </div>
                <div>
                <div class="btn-group" role="group" aria-label="Basic example">
                    {{--  <button type="button" class="btn btn-mini btn-outline-primary" data-toggle="collapse" data-target="#collapseAminities- i}}">Aminities</button>  --}}
                    <button type="button" class="btn btn-mini btn-outline-primary" data-toggle="collapse" data-target="#collapseBoardingDrop-{{$i}}">Boarding &amp; Dropping Point</button>
                    <button type="button" class="btn btn-mini btn-outline-primary" data-toggle="collapse" data-target="#collapseCancellation-{{$i}}">Cancellation Policy</button>
                </div>
                </div>
            </div>
        </div>

        <div class="collapse" id="collapseBoardingDrop-{{$i}}">
            <div class="tm-aminities-list tm-aminities-list--spacious-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <span class="badge badge-pill badge-info badge-custom--mini color-lighter">BOARDING POINT</span>
                            <div class="search-details search-boarding">
                                @foreach($bus['boardingPoints'] as $boardingPoint)
                                <p><span><strong>{{$boardingPoint['time']}} </strong>{{$boardingPoint['location']}}</span></p>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <span class="badge badge-pill badge-info badge-custom--mini color-lighter">DROPPING POINT</span>
                            <div class="search-details search-boarding">
                                @if(count($bus['droppingPoints']))
                                @foreach($bus['droppingPoints'] as $droppingPoint)
                                <p><span><strong>{{$droppingPoint['time']}} </strong>{{$droppingPoint['location']}}</span></p>
                                @endforeach
                                @else
                                <p><span><strong>{{$bus['arrivalTime']}} </strong>{{$destination}}</span></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="collapse" id="collapseCancellation-{{$i}}">
            <div class="tm-aminities-list tm-aminities-list--spacious-wrapper">
                <div class="container">
                    <label for="collapseCancellation " class="custom-label-search">CANCELLATION</label>
                    <div class="row text-center">
                        <div class="col-xs-12 col-md-6">
                            <span class="badge badge-pill badge-info badge-custom--mini color-lighter">Cut Off Time (Before Dep Hours)</span>
                            <div class="search-details search-boarding">
                                @php
                                    $data = json_decode($bus['cancellationPolicy'],true);
                                    $data[] = ['cutoffTime'=>24,'refundInPercentage'=>''];
                                @endphp
                                @for ($j = 0; $j < count($data)-1; $j++)
                                    <p><span>{{$data[$j]['cutoffTime']}} - {{$data[$j+1]['cutoffTime']}}</span></p>
                                @endfor
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <span class="badge badge-pill badge-info badge-custom--mini color-lighter">Deduction Percentage</span>
                            <div class="search-details search-boarding">
                                @for ($j = 0; $j < count($data)-1; $j++)
                                    <p><span>{{$data[$j]['refundInPercentage']}}%</span></p>
                                @endfor
                            </div>
                        </div>
                     </div>
                    {{-- <div class="row">
                        <strong class="note-text">Partial cancellation allowed. Please note that primary passenger booking cannot be cancelled</strong>
                    </div>  --}}
                </div>
            </div>
        </div>

        <div class="collapse seatcontent" id="collapseViewSeats-{{$i}}">
            <div class="tm-aminities-list tm-aminities-list--spacious-wrapper">
                <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="seat-leagend">
                          <ul>
                            <li><span class="seat seat--available"></span>Available</li>
                            <li><span class="seat seat--ladies"></span>Available for female</li>
                            <li><span class="seat seat--booked"></span>Booked</li>
                            <li><span class="seat seat--unavailable"></span>Unavailable</li>
                          </ul>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="tm-seat-layout-wrapper" id="seatlayout-{{$bus['routeScheduleId']}}">
                        </div>
                    </div>
                    {{--  <div class="col-xs-12 col-md-6">
                        <span class="badge badge-pill badge-info badge-custom--mini color-lighter">Booking Offers</span>
                        <div class="tm-offer-wrapper">
                        <ul>
                            <li>
                            <img src="https://aniportalimages.s3.amazonaws.com/media/details/Paytm_1.jpg" alt="">
                            <div class="content">
                                Offer Listing
                            </div>

                            </li>
                            <li>
                            <img src="https://s1.rdbuz.com/trips/images/redBus_logo_red.png" alt="">
                            <div class="content">
                                Offer Listing1
                            </div>
                            </li>
                            <li>
                            <img src="https://upload.wikimedia.org/wikipedia/en/a/a9/KSRTC_logo.png" alt="">
                            <div class="content">
                                Offer Listing2
                            </div>
                            </li>
                            <li>
                            <img src="http://www.sumarpark.com/images/logo-vrl.png" alt="">
                            <div class="content">
                                Offer Listing3
                            </div>
                            </li>
                            <li>
                            <img src="https://gadgetstouse.com/wp-content/uploads/2017/07/Ola.png" alt="">
                            <div class="content">
                                Offer Listing4
                            </div>
                            </li>
                            <li>
                            <img src="http://rackons.com/oc-content/themes/veronika/images/partner-logos/freecharge.png" alt="">
                            <div class="content">
                                Offer Listing5
                            </div>
                            </li>
                            <li>
                            <img src="http://oladesignworks.com/wp-content/uploads/2015/04/olalogoforhome-e1427912710675.png" alt="">
                            <div class="content">
                                Offer Listing6
                            </div>
                            </li>
                            <li>
                            <img src="http://image3.mouthshut.com/images/imagesp/925069804s.png" alt="">
                            <div class="content">
                                Offer Listing7
                            </div>
                            </li>
                        </ul>
                        </div>
                    </div>  --}}
                </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @php
    }
    else{
      @endphp
      <div class="tmm-no-result">
          <span class="no-result-icon"><i class="fa fa-info-circle"></i></span>
          <p>No Trips Available on this date</p>
      </div>
      @php
    }
    @endphp

    @php
    $i = 0;
    @endphp
    @foreach($tickets as $ticket)
    @php
    $i++;
    @endphp
    <div>
        <div class="search-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <span class="badge badge-pill badge-default badge-custom">Bus Details</span>
                        <div class="search-details">
                        <p><strong>{{$ticket->bus_operator->name}}</strong></p>
                            <p>{{$ticket->bus_type->name}}</p>
                        </div>

                    </div>
                    <div class="col-xs-12 col-md-4 u-p-relative">
                        <span class="badge badge-pill badge-default badge-custom">Boarding Details</span>
                        <div class="search-details">
                            <p><strong>{{date('h:i A', strtotime($ticket->journey_time))}}</strong></p>
                            {{--  <p>Anand Rao Junction</p>  --}}
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 u-text-right">
                        <div class="sub-details-mobile-tbt">
                            <span class="badge badge-pill badge-danger badge-custom">Fare</span>
                            <div class="search-details">
                                <p><strong class="u-f-large">INR {{$ticket->price}}.00</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12 u-p-relative">
                        <p class="tmm-more"></p>
                        <div class="btn-mini-result-wrapper">
                            <button type="button" class="btn btn-mini-result btn-outline-primary"
                            @if(!Auth::check())
                                onclick="document.location.href='{{route('login')}}'"
                            @else
                                data-toggle="collapse" data-target="#collapseContactDetails-{{$i}}" aria-expanded="false" aria-controls="collapseContactDetails-{{$i}}"
                            @endif
                            >View Contact Details</button>
                        <button type="button" onclick="setTicketId({{$ticket->id}})" class="btn btn-mini-result btn-outline-primary" data-toggle="modal" data-target="#requestTicketModal">Request Ticket Via SMS</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse collapse-details" id="collapseContactDetails-{{$i}}">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        <span class="badge badge-pill badge-info badge-custom--mini">Name</span>
                        <div class="search-details">
                            <p><span>{{$ticket->user->name}}</span></p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <span class="badge badge-pill badge-info badge-custom--mini">Contact Number</span>
                        <div class="search-details">
                            <p><span>+91-{{$ticket->user->mobile}}</span></p>
                        </div>
                    </div>
                    {{--  <div class="col-xs-12 col-md-3">
                        <span class="badge badge-pill badge-info badge-custom--mini"><i class="fa fa-map-marker" aria-hidden="true"></i> Place</span>
                        <div class="search-details">
                            <p><span>Bangalore, Karnataka</span></p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <span class="badge badge-pill badge-info badge-custom--mini">Number Verified</span>
                        <div class="search-details">
                            <p><span class="badge badge-danger">Invalid Number</span></p>
                        </div>
                    </div>  --}}
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @if($i==0)
    <div class="tmm-no-result">
        <span class="no-result-icon"><i class="fa fa-info-circle"></i></span>
        <p>No Trips Available on this date</p>
    </div>
    @endif

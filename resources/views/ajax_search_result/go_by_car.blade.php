    @php
    $k = 0;
    @endphp
    @foreach($trips as $trip)
    @php
    $k++;
    @endphp
    <div>
        <div class="search-wrapper">
            <div class="container">
            <div class="row mb-3">
                <div class="col-xs-12 col-md-4">
                   <div class="search-details">
                        <!-- <p><strong>S.P Balamurgan</strong></p> -->
                        <!-- <p>33 Years Old</p> -->
                        <div class="search-details--user">
                          @if(!empty($trip['car']))
                            <img src="{{$trip['car']['picture']}}" alt="User" class="rounded-circle">
                          @endif
                            <!-- <span class="id-proof badge badge-success"><i class="fa fa-check-circle" aria-hidden="true"></i> Govt. ID Verified</span> -->
                        </div>
                        <div class="search-details--social-count">
                            <!-- <i class="fa fa-facebook-square"></i><span>234 Friends</span> -->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5">
                    <div class="search-details">
                    <p>
                        <span>{{$trip['departure_place']['city_name']}}</span>
                        <span class="u-p-l-r-small"><i class="icon icon-arrows-slim-right"></i></span>
                        <span>{{$trip['arrival_place']['city_name']}}</span>
                    </p>
                    <div class="source-destination">
                        <p class="source-destination--source">
                        <i class="fa fa-map-marker fa-lg"></i><span>{{$trip['departure_place']['address']}}</span>
                        </p>
                        <p class="source-destination--destination">
                        <i class="fa fa-map-marker fa-lg"></i><span>{{$trip['arrival_place']['address']}}</span>
                        </p>
                    </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="search-details u-text-right">
                    <div class="sub-details-mobile">
                        <p><strong class="u-f-large">INR {{$trip['price']['value']}}</strong></p>
                        <p><small>Per co Traveller</small></p>
                        <p><small class="badge badge-info">{{$trip['seats_left']}} Seats Left</small></p>
                    </div>
                    </div>
                    <div class="btn-group right-align-button" role="group" aria-label="Action">
                    <button type="button" class="btn btn-outline-primary btn-mini"
                    @if(!Auth::check())
                    onclick="document.location.href='{{url('/login')}}'"
                    @else
                    data-toggle="collapse" data-target="#collapseCardetails-{{$k}}" aria-expanded="false" aria-controls="collapseCardetails-{{$k}}"
                    @endif
                    >More Details</button>
                    <button type="button" class="btn btn-danger btn-mini pull-right" onclick="window.open('{{$trip['links']['_front']}}','_blank')">Book</button>
                    </div>
                </div>
            </div>
            </div>

        </div>
        <div class="collapse collapse-details" id="collapseCardetails-{{$k}}">
            <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    {{--  <span class="badge badge-pill badge-info badge-custom--mini">Options</span>
                    <div class="search-details">
                    <p><span>Max 2 People in the Back seatallowed </span></p>
                    </div>  --}}
                </div>
                <div class="col-xs-12 col-md-4">
                    {{--  <span class="badge badge-pill badge-info badge-custom--mini">co-Travellers on this Ride</span>
                    <div class="search-details">
                        <ul class="co-traveller">
                            <li >
                            <img data-toggle="tooltip" data-placement="bottom" title="Roshan Kumar Singh" src="./src/img/Circle-icons-profle.svg" alt="co-traveller">
                            </li>
                            <img data-toggle="tooltip" data-placement="bottom" title="Roshan Kumar Singh" src="./src/img/Circle-icons-profle.svg" alt="co-traveller">
                            <li >
                            </li>
                            <li >
                            <img data-toggle="tooltip" data-placement="bottom" title="Roshan Kumar Singh" src="./src/img/Circle-icons-profle.svg" alt="co-traveller">
                            </li>
                            <li >
                            <img data-toggle="tooltip" data-placement="bottom" title="Roshan Kumar Singh" src="./src/img/Circle-icons-profle.svg" alt="co-traveller">
                            </li>
                        </ul>
                    </div>  --}}
                </div>
                <div class="col-xs-12 col-md-4">
                    <span class="badge badge-pill badge-info badge-custom--mini">Car Details</span>
                    <div class="search-details">
                        @if(!empty($trip['car']))
                        <p><span>{{$trip['car']['model']}}</span></p>
                        <p><span>{{$trip['car']['color']}}</span></p>
                        <p><span>Reg. No. : <strong>KA 05KA 5215</strong></span></p>
                        @else
                        <p><span>Not Provided</span></p>
                        @endif
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    @endforeach
    @if($k==0)
    <div class="tmm-no-result">
        <span class="no-result-icon"><i class="fa fa-info-circle"></i></span>
        <p>No Trips Available on this date</p>
    </div>
    @endif

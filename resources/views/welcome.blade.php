@extends('layouts.app')

@section('content')    
    <header class="header" id="header">
        <div class="header__text-box col-1-of-1">
            <h1 class="heading-primary clerfix">
            <span class="heading-primary--main">One Search for all your travel needs with best discount</span>
            <span class="heading-primary--sub">Don't loose 100% on cancellation</span>
            </h1>
        </div>
    </header>

    <div class="container u-no-left-right-padding" ng-controller="mainController">
        <section class="section-about">
        <div id="search">
            <form action="{{url('/searchresult')}}" id="search_form" method="post">
                {{csrf_field()}}
                <div class="clearfix search-wrap">
                    <div class="fl search-box clearfix">
                        <span class="fl icontmm-city icontmm">
                        </span>
                        <div>
                            <div style="width: 180px;position: absolute;bottom: 0;right: 11px;">
                                <input name="source_id" class="form-control" id="source" required>
                            </div>
                            <label for="source" class="db move-up search-label-position" >FROM</label>
                            <div class="error-message-fixed "></div>
                        </div>
                    </div>
                    <span class="icontmm-doublearrow icon" id="togglebtn" onclick="swipeCity();"></span>
                    <div class="fl search-box">
                        <span class="fl icontmm-city icontmm icontmm-city--position">
                        </span>
                        <div>
                            <div style="width: 180px;position: absolute;bottom: 0;right: 0px;">
                                <input name="destination_id" class="form-control" id="destination" required>
                            </div>
                            <label for="destination"  class="db move-up search-label-position">TO</label>
                            <div class="error-message-fixed "> </div>
                        </div>
                    </div>
                    <div class="fl search-box date-box gtm-onwardCalendar u-p-relative">
                        <span class="fl icontmm-calendar_icontmm-new icontmm">
                        </span>
                        <div>
                            <div id="datepicker" class="input-group date" style="width: 133px;height: 24px;
                            position: absolute;right: 1px;bottom: 1px;" data-date-format="yyyy-mm-dd">
                                <input class="form-control" name="journey-date" type="text" readonly style="    width: 100px;
                                height: 24px;"/>
                                <span class="input-group-addon" style="font-size:17px"><i class="fa fa-calendar"></i></span>
                            </div>
                            <label for="onward_cal" class="db text-trans-uc move-up">Onward Date</label>
                        </div>
                    </div>
                    <button  id="search_btn">Search</button>
                </div>
            </form>
        </div>
        </section>
    </div>

    <div class="section-cta-bg u-no-left-right-padding">
        <section>
            <div class="container landing-feature-wrapper">
            <div class="row section-features">
                <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="feature-box feature-box--brand-bg feature-box--rounded feature-box--header">
                    <div class="feature-box--image-wrapper">
                    <img src="{{{ URL::asset('img/bus-ticket.jpg')}}}" alt="Bus Ticket Booking">
                    <span>Bus Ticket Booking</span>
                    </div>
                    <p class="feature-box__text feature-box__text--darker">
                    Search, compare discount &amp; book at lowest discount platform
                    </p>
                    <p class="feature-box__text feature-box__text--darker">
                    Maximum platforms who provides discount are covered
                    </p>

                    
                </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="feature-box feature-box--brand-bg feature-box--rounded feature-box--header">
                    <div class="feature-box--image-wrapper">
                    <img src="{{{ URL::asset('img/ticket-transfer.jpg')}}}" alt="Transferable Bus Ticket">
                    <span>Transferable Bus Ticket</span>
                    </div>
                    <p class="feature-box__text feature-box__text--darker">
                    Transfer your ticket to a desparate traveller at operator's discretion
                    </p>
                    
                </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                <div class="feature-box feature-box--brand-bg feature-box--rounded feature-box--header">
                    <div class="feature-box--image-wrapper">
                    <img src="{{{ URL::asset('img/car-pool.jpg')}}}" alt="Go by Car">
                    <span>Go by Shared car</span>
                    </div>
                    <p class="feature-box__text feature-box__text--darker">
                    Go green! Go Quick!! Go together in a car!!!
                    </p>
                    
                </div>
                </div>

            </div>
            </div>
        </section>
    </div>

    <div class="container">
        <section class="customer-logos client-slider">
            <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
            <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
         </section>
    </div>

    <section class="section-stories">
        <div class="container">
            <div class="u-center-text u-margin-bottom-medium">
                <h2 class="heading-testimonials">
                    What our customers say
                </h2>
            </div>
            <div class="row touch-slider owl-carousel owl-theme">
                <div>
                    <div class="customer-say-content">
                        <div class="customer-say-feedback">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</div>
                        <div class="customer-arrow-down"></div>
                        <div class="customer-info">
                            <div class="customer-image">
                                <img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png">
                            </div>
                            <div class="customer-details">
                                <span class="customer-name">Loren Ipsum, MD</span>
                                <span class="customer-designation">CEO at TicketMeMe Pvt. Ltd.</span>
                            </div>
                        </div>             
                    </div>
                </div>
                <div>
                    <div class="customer-say-content">
                        <div class="customer-say-feedback">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</div>
                        <div class="customer-arrow-down"></div>
                        <div class="customer-info">
                            <div class="customer-image">
                                <img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png">
                            </div>
                            <div class="customer-details">
                                <span class="customer-name">Loren Ipsum, MD</span>
                                <span class="customer-designation">CEO at TicketMeMe Pvt. Ltd.</span>
                            </div>
                        </div>             
                    </div>
                </div>
                <div>
                    <div class="customer-say-content">
                        <div class="customer-say-feedback">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</div>
                        <div class="customer-arrow-down"></div>
                        <div class="customer-info">
                            <div class="customer-image">
                                <img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png">
                            </div>
                            <div class="customer-details">
                                <span class="customer-name">Loren Ipsum, MD</span>
                                <span class="customer-designation">CEO at TicketMeMe Pvt. Ltd.</span>
                            </div>
                        </div>             
                    </div>
                </div>
                <div>
                    <div class="customer-say-content">
                        <div class="customer-say-feedback">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</div>
                        <div class="customer-arrow-down"></div>
                        <div class="customer-info">
                            <div class="customer-image">
                                <img src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png">
                            </div>
                            <div class="customer-details">
                                <span class="customer-name">Loren Ipsum, MD</span>
                                <span class="customer-designation">CEO at TicketMeMe Pvt. Ltd.</span>
                            </div>
                        </div>             
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer_script')
<script>
    (function () {
        $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
        }).datepicker('update', new Date());
        $('#source').autocomplete({
            serviceUrl: '/search-city',
            'autoSelectFirst': true
        });
        $('#destination').autocomplete({
            serviceUrl: '/search-city',
            'autoSelectFirst': true
        });
        @if(Session::has('message'))
            @if(Session::get('message')=='register')
                swal("Welcome!", "You have successfully registered", "success");
            @elseif(Session::get('message')=='ticketpost')
                swal("Ticket Posted!",'{{Session::get('text')}}', "success");
            @elseif(Session::get('message')=='subscribe')
                swal("Welcome!",'Thanks for your subscription', "success");
            @elseif(Session::get('message')=='feedback')
                swal("Welcome!",'Thanks for your feedback', "success");
            @elseif(Session::get('message')=='requestTicket')
                swal("Welcome!",'{{Session::get('text')}}', "success");
            @endif
        @endif
    })();
    function swipeCity()
    {
        var source = $('#source').val();
        var destination = $('#destination').val();
        $('#destination').val(source);
        $('#source').val(destination);
    }
</script>
@endsection
@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Settings</h1>
    <p>Setting page for admin</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile user-settings">
      <h4 class="line-head">Update UserName and Email</h4>
      <form action="{{route('admin.update.name.email')}}" method="post">
        {{csrf_field()}}
        <div class="row mb-4">
          <div class="col-md-12">
            <label>User Name</label>
            <input class="form-control" name="username" value="{{session('admin_username')}}" type="text" required>
          </div>
          <div class="col-md-12">
            <label>Email</label>
            <input class="form-control" name="email" value="{{session('admin_email')}}" type="email" required>
          </div>
        </div>
        <div class="row mb-10">
          <div class="col-md-12">
            <button class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
          </div>
        </div>
      </form>
    </div>
    <div class="tile user-settings">
      <h4 class="line-head">Update Password</h4>
      <form method="post" action="{{route('admin.update.password')}}">
        {{csrf_field()}}
        <div class="row mb-4">
          <div class="col-md-12">
            <label>Current Password</label>
            <input class="form-control" name="current_password" type="password" required>
            @if(Session::has('current_password'))
              <p class="text-danger">{{Session::get('current_password')}}</p>
            @endif
          </div>
          <div class="col-md-12">
            <label>New Password</label>
            <input class="form-control" name="new_password" type="password" required>
          </div>
          <div class="col-md-12">
            <label>Confirm New Password</label>
            <input class="form-control" name="new_password_confirmation" type="password" required>
            @if($errors->has('new_password'))
              <p class="text-danger">{{$errors->first('new_password')}}</p>
            @endif
          </div>
        </div>
        <div class="row mb-10">
          <div class="col-md-12">
            <button class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

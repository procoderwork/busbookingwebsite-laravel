@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Edit Ticket</h1>
    <p>Edit Ticket that {{$ticket->user->name}} posted at {{date('jS M,Y h:i A',strtotime($ticket->created_at))}}.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.ticket.index')}}">Manage Ticket Posting</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.ticket.edit',$ticket->id)}}">Edit Ticket</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form method="post" action="{{route('admin.ticket.update',$ticket->id)}}">
          {{csrf_field()}}
          <input type="hidden" name="_method" value="PUT">
          <div class="row mb-4">
            <div class="col-md-4">
              <label>Source City</label>
              <select class="form-control" name="source_id" id="source" required>
                <option value="{{$ticket->source->id}}" selected>{{$ticket->source->name}}</option>
              </select>
            </div>
            <div class="col-md-4">
              <label>Destination City</label>
              <select class="form-control" name="destination_id" id="destination" required>
                <option value="{{$ticket->destination->id}}" selected>{{$ticket->destination->name}}</option>
              </select>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col-md-4">
              <label>Journey Date</label>
              <div class="form-group">
                  <div class="input-group date" id="datepicker" data-target-input="nearest">
                      <input name="journey_time" type="text" id="journey-date" required class="form-control datetimepicker-input" data-target="#datepicker"/>
                      <div class="input-group-append" data-target="#datepicker" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-4">
              <label>Price</label>
              <input class="form-control" type="text" name="price" value="{{$ticket->price}}" required>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col-md-4">
              <label>Bus Operator</label>
              <select class="form-control" name="bus_operator_id" required>
                @foreach(App\BusOperator::all() as $bus_operator)
                <option value="{{$bus_operator->id}}" @if($ticket->bus_operator_id == $bus_operator->id) selected @endif>{{$bus_operator->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-4">
              <label>Bus Type</label>
              <select class="form-control" name="bus_type_id" required>
                @foreach(App\BusType::all() as $bus_type)
                <option value="{{$bus_type->id}}" @if($ticket->bus_type_id == $bus_type->id) selected @endif>{{$bus_type->name}}</option>
                @endforeach
              </select>
            </div>
          </div>

          </div>
          <div class="row mb-10">
            <div class="col-md-12">
              <button class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('additional_css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/css/tempusdominus-bootstrap-4.min.css" />
@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript">
(function () {
    $("#datepicker").datetimepicker({
        defaultDate:new Date('{{$ticket->journey_time}}')
    });
    $('#destination').select2({
        placeholder:'Select Destination City',
        ajax: {
            dataType: 'json',
            url: '{{route('search-city1')}}',
            processResults: function (data) {
                // Tranforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: data.items
                };
            }
        }
    });
    $('#source').select2({
        placeholder:'Select Source City',
        ajax: {
            dataType: 'json',
            url: '{{route('search-city1')}}',
            processResults: function (data) {
                // Tranforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: data.items
                };
            }
        }
    });
})();
</script>
@endsection

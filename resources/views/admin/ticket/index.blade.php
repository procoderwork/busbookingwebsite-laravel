@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Manage Tickets</h1>
    <p>Manage tickets of all users</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.ticket.index')}}">Manage Ticket Posting</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <table class="table table-hover table-bordered" id="sampleTable">
          <thead>
            <tr>
              <th>Posted Date</th>
              <th>Posted By</th>
              <th>Journey Date</th>
              <th>Source City</th>
              <th>Destination City</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($tickets as $ticket)
            <tr>
              <td>{{date('jS M,Y h:i A',strtotime($ticket->created_at))}}</td>
              <td>{{$ticket->user->name}}</td>
              <td>{{date('jS M,Y h:i A',strtotime($ticket->journey_time))}}</td>
              <td>{{$ticket->source->name}}</td>
              <td>{{$ticket->destination->name}}</td>
              <td><div class="toggle"><label><input id="status_{{$ticket->id}}" onclick="setStatus('status_{{$ticket->id}}')" type="checkbox" {{($ticket->status?'checked':'')}}><span class="button-indecator"></span></label></div></td>
              <td>
                <a class="btn btn-primary text-light" href="{{route('admin.ticket.edit',$ticket->id)}}"><i class="fa fa-edit"></i> Edit</a>
                <a class="btn btn-danger text-light" href="#" onclick="onDelete('{{route('admin.ticket.destroy',$ticket->id)}}')"><i class="fa fa-trash"></i> Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<form id="delete_form" method="post">
  <input type="hidden" name="_method" value="DELETE">
  {{csrf_field()}}
</form>
@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
<script type="text/javascript">
  function setStatus(id)
  {
    $.ajax({
      url:'{{route('admin.ticket.update_status')}}',
      type:'POST',
      data:{'id':id.split('_')[1],'_token':'{{csrf_token()}}','data':$('#'+id).is(':checked')},
      success:function(result){

      }
    });
  }
  function onDelete(url)
  {
    $('#delete_form').attr('action',url);
    $('#delete_form').submit();
  }
</script>
@endsection

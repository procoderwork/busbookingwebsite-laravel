
@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Manage Faqs</h1>
    <p>Manage all faqs in this website.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.ticket.index')}}">Manage Faqs</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <a class="btn btn-primary text-light" href="{{route('admin.faq.add')}}" style="margin-bottom: 10px"><i class="fa fa-plus"></i> Add New Faq</a>
        <table class="table table-hover table-bordered" id="sampleTable">
          <thead>
            <tr>
              <th>Question</th>
              <th>Created At</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($faqs as $faq)
            <tr>
              <td>{{$faq->question}}</td>
              <td>{{date('jS M,Y h:i A',strtotime($faq->created_at))}}</td>
              <td>
                <a class="btn btn-primary text-light" href="{{route('admin.faq.edit',$faq->id)}}"><i class="fa fa-edit"></i> Edit</a>
                <a class="btn btn-danger text-light" onclick="destroy('{{route('admin.faq.destroy',$faq->id)}}')"><i class="fa fa-trash"></i> Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/bootstrap-notify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/sweetalert.min.js')}}"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
<script type="text/javascript">
function destroy(url){
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this feedback!",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No, cancel plx!",
    closeOnConfirm: false,
  }, function(isConfirm) {
    if (isConfirm) {
      document.location.href = url;
    }
  });
}
</script>
@endsection

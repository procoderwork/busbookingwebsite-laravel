@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Edit Faq</h1>
    <p>Edit This Faq.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.faq.index')}}">Manage Faqs</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.faq.edit',$faq->id)}}">Edit Faq</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form method="post" action="{{route('admin.faq.update',$faq->id)}}">
          {{csrf_field()}}
          <div class="row mb-4">
            <div class="col-md-12">
              <label for="question">Question</label>
              <input class="form-control" type="text" name="question" id="question" value="{{$faq->question}}">
            </div>
            <div class="col-md-12">
              <label for="answer">Answer</label>
              <textarea class="form-control" name="answer" id="answer" style="height:200px"  >{{$faq->answer}}</textarea>
            </div>
          </div>
           <div class="row mb-10">
            <div class="col-md-12">
              <button class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
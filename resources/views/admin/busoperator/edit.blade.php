@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Edit Bus Operator </h1>
    <p>Edit bus operator.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.busoperator.index')}}">Manage Bus Types</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.busoperator.edit',$busoperator->id)}}">Edit Bus Type</a></li>
  </ul>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="tile">
  			<div class="tile-body">
  				<form method="POST" action="{{route('admin.busoperator.update',$busoperator->id)}}">
  					{{csrf_field()}}
  					<div class="row" style="margin-bottom: 10px">
  						<div class="col-md-2">
  							<label for="bus_type_name" class="align-right">
  								Bus Operator Name
  							</label>
  						</div>
  						<div class="col-md-8">
  							<input type="text" name="name" value="{{$busoperator->name}}" id="bus_type_name" class="form-control">
  						</div>
  						<button class="btn btn-success col-md-1">Edit</button>
  					</div>
  				</form>
  			</div>
  		</div>
  	</div>
</div>
@endsection
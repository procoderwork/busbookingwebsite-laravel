
@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Manage Subscribes</h1>
    <p>Manage all subscribes in this website.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.ticket.index')}}">Manage Subscribes</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <table class="table table-hover table-bordered" id="sampleTable">
          <thead>
            <tr>
              <th>Username</th>
              <th>Mobile</th>
              <th>Created At</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($subscribes as $subscribe)
            <?php
            	$user = App\User::where('mobile',$subscribe->mobile)->get();
            	if(count($user))
            		$user = $user[0]->name;
            	else
            		$user = 'Unknown User';
            ?>
            <tr>
              <td>{{$user}}</td>
              <td>{{$subscribe->mobile}}</td>
              <td>{{date('jS M,Y h:i A',strtotime($subscribe->created_at))}}</td>
              <td>
                <a class="btn btn-danger text-light" onclick="destroy('{{route('admin.subscribe.destroy',$subscribe->id)}}')"><i class="fa fa-trash"></i> Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/bootstrap-notify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/sweetalert.min.js')}}"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
<script type="text/javascript">
function destroy(url){
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this feedback!",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No, cancel plx!",
    closeOnConfirm: false,
  }, function(isConfirm) {
    if (isConfirm) {
      document.location.href = url;
    }
  });
}
</script>
@endsection

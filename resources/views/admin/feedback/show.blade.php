@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Manage Feedback</h1>
    <p>Manage all feedback in this website.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.ticket.index')}}">Manage Feedback</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form>
          <div class="row">
            <div class="col-md-6">
              <label>Name</label>
              <input type="text" class="form-control" value="{{$feedback->name}}">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Email</label>
              <input type="text" class="form-control" value="{{$feedback->email}}">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Mobile</label>
              <input type="text" class="form-control" value="{{$feedback->mobile}}">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label>Feedback</label>
              <textarea class="form-control" id="exampleTextarea" rows="3">{{$feedback->comment}}</textarea>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

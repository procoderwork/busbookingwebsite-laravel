@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Manage Users</h1>
    <p>Manage all users logged in this website.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.user.index')}}">Manage Users</a></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <table class="table table-hover table-bordered" id="sampleTable">
          <thead>
            <tr>
              <th>Username</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Registered Date</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->mobile}}</td>
              <td>{{date('jS M,Y h:i A',strtotime($user->created_at))}}</td>
              <td><div class="toggle"><label><input id="status_{{$user->id}}" onclick="setStatus('status_{{$user->id}}')" type="checkbox" {{($user->is_enabled?'checked':'')}}><span class="button-indecator"></span></label></div></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
<script type="text/javascript">
function setStatus(id)
{
  $.ajax({
    url:'{{route('admin.user.update_status')}}',
    type:'POST',
    data:{'id':id.split('_')[1],'_token':'{{csrf_token()}}','data':$('#'+id).is(':checked')},
    success:function(result){

    }
  });
}
</script>
@endsection

@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Edit Bus Type </h1>
    <p>Edit bus type.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.bustype.index')}}">Manage Bus Types</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.bustype.edit',$bustype->id)}}">Edit Bus Type</a></li>
  </ul>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="tile">
  			<div class="tile-body">
  				<form method="POST" action="{{route('admin.bustype.update',$bustype->id)}}">
  					{{csrf_field()}}
  					<div class="row" style="margin-bottom: 10px">
  						<div class="col-md-2">
  							<label for="bus_type_name" class="align-right">
  								Bus Type Name
  							</label>
  						</div>
  						<div class="col-md-8">
  							<input type="text" name="name" value="{{$bustype->name}}" id="bus_type_name" class="form-control">
  						</div>
  						<button class="btn btn-success col-md-1">Edit</button>
  					</div>
  				</form>
  			</div>
  		</div>
  	</div>
</div>
@endsection
@extends('layouts.adminapp')

@section('content')
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> Manage Bus Types </h1>
    <p>Manage all bus types in this website.</p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i>Setting</li>
    <li class="breadcrumb-item"><a href="{{route('admin.bustype.index')}}">Manage Bus Types</a></li>
  </ul>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="tile">
  			<div class="tile-body">
  				<form method="POST" action="{{route('admin.bustype.insert')}}">
  					{{csrf_field()}}
  					<div class="row" style="margin-bottom: 10px">
  						<div class="col-md-2">
  							<label for="bus_type_name" class="align-right">
  								Bus Type Name
  							</label>
  						</div>
  						<div class="col-md-8">
  							<input type="text" name="name" id="bus_type_name" class="form-control" required="">
  						</div>
  						<button class="btn btn-success col-md-1">Add New</button>
  					</div>
  				</form>
  				<table class="table table-hover table-bordered" id="sampleTable">
		          <thead>
		            <tr>
		              <th>Name</th>
		              <th>Created At</th>
		              <th>Action</th>
		            </tr>
		          </thead>
		          <tbody>
		            @foreach($bustypes as $bustype)
		            <tr>
		              <td>{{$bustype->name}}</td>
		              <td>{{date('jS M,Y h:i A',strtotime($bustype->created_at))}}</td>
		              <td>
		                <a class="btn btn-primary text-light" href="{{route('admin.bustype.edit',$bustype->id)}}"><i class="fa fa-edit"></i> Edit</a>
		                <a class="btn btn-danger text-light" onclick="destroy('{{route('admin.bustype.destroy',$bustype->id)}}')"><i class="fa fa-trash"></i> Delete</a>
		              </td>
		            </tr>
		            @endforeach
		          </tbody>
		        </table>
  			</div>
  		</div>
  	</div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/bootstrap-notify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin_assets/js/plugins/sweetalert.min.js')}}"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
<script type="text/javascript">
function destroy(url){
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this feedback!",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No, cancel plx!",
    closeOnConfirm: false,
  }, function(isConfirm) {
    if (isConfirm) {
      document.location.href = url;
    }
  });
}
</script>
@endsection

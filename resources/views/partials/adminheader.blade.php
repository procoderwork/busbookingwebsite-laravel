<!-- Admin Header -->
<header class="app-header">
  <a class="app-header__logo" href="/admin/home">
    <img src="{{{ URL::asset('img/ticketmeme.png')}}}" alt="ticketmeme">
  </a>
  <!-- Sidebar toggle button-->
  <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
  <!-- Navbar Right Menu-->
  <ul class="app-nav">
    <li class="app-search">
      <input class="app-search__input" type="search" placeholder="Search">
      <button class="app-search__button"><i class="fa fa-search"></i></button>
    </li>

    <!-- User Menu-->
    <li>
      <a class="app-nav__item" href="/" target="_new">
          <i class="fa fa-eye fa-lg"></i></a>
      </a>
    </li>
        <li>
      <a class="app-nav__item" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          <i class="fa fa-power-off fa-lg"></i></a>
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
    </li>
  </ul>
</header>
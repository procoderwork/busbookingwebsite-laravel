
<footer class="footer">
    <div class="footer__content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-4">
                    <ul class="list-unstyled">
                    <li ng-click="scrollToTop();"><a href="/termsofuse">Terms &amp; Conditions</a></li>
                    <li ng-click="scrollToTop();"><a href="/privacy">Privacy Policy</a></li>
                    <li ng-click="scrollToTop();"><a href="/faq">FAQs</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-4">
                    <span>We are Social :</span>
                    <ul class="list-inline">
                        <li class="list-inline-item"><a class="color-google" href=""></a></li>
                        <li class="list-inline-item"><a class="color-facebook" href=""></a></li>
                        <li class="list-inline-item"><a class="color-twitter" href=""></a></li>
                        <li class="list-inline-item"><a class="color-linkedin" href=""></a></li>
                        <li class="list-inline-item"><a class="color-insta" href=""></a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-4">
                    <span>Subscribe for Updates :</span>
                    <form action="{{route('subscribe')}}" method="post">
                        {{csrf_field()}}
                        <div class="input-group">
                            <input type="text"
                            @if(Auth::check())
                            value="{{Auth::user()->mobile}}"
                            @endif
                            class="form-control" required name="phone" placeholder="Subscribe Phone" id="subscribe">
                            <span class="input-group-btn" style="width: unset;">
                                <button class="btn btn-primary" type="submit">Subscribe</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row u-center-text">
            <div class="u-align-center">&copy; 2018 TicketMeMe, All Rights Reserved</div>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="feedback">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Feedback</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="icon-arrows-remove"></i></button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        <form action="{{route('feedback')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name <small>(Required)</small> :</label>
                <input type="text" name="name"
                @if(Auth::check())
                value="{{Auth::user()->name}}"
                @endif
                 class="form-control" id="name" required>
            </div>

            <div class="form-group">
                <label for="email">Email Address:</label>
                <input type="email"
                @if(Auth::check())
                value="{{Auth::user()->email}}"
                @endif
                 name="email" class="form-control" id="email" required>
            </div>

            <div class="form-group">
                <label for="mobile">Mobile Number:</label>
                <input type="text"
                @if(Auth::check())
                value="{{Auth::user()->mobile}}"
                @endif
                class="form-control" name="mobile" id="mobile" required>
            </div>
            <div class="form-group">
                <label for="comment">Your Feedback <small>(Required)</small>:</label>
                <textarea class="form-control" name="comment" rows="5" id="comment" required></textarea>
            </div>

            <button type="submit" class="btn btn-primary btn-tmm-default">Send Feedback</button>
            <button type="submit" onclick="event.preventDefault()" class="btn btn-default btn-tmm-default">Cancel</button>
        </form>
        </div>
        </div>
    </div>
</div>
<div class="modal fade" id="requestTicketModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Request This Ticket</h4>
                <button type="button" class="close" data-dismiss="modal"><i class="icon-arrows-remove"></i></button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
            <form action="{{route('request-ticket')}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" id="ticket_id" name="ticket_id">
                <div class="form-group">
                    <label for="mobile">Mobile Number:</label>
                    <input type="text"
                    @if(Auth::check())
                    value="{{Auth::user()->mobile}}"
                    @endif
                    class="form-control" name="mobile" id="ticket_mobile" required>
                </div>
                <div class="form-group">
                    <label for="message">Your Message <small>(Required)</small>:</label>
                    <textarea class="form-control" name="message" rows="5" id="message" required></textarea>
                </div>

                <button type="submit" class="btn btn-primary btn-tmm-default">Request This Ticket</button>
                <button type="submit" onclick="event.preventDefault();$('#requestTicketModal').modal('hide')" class="btn btn-default btn-tmm-default">Cancel</button>
            </form>
            </div>
            </div>
        </div>
    </div>

<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
  <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="{{{ URL::asset('img/man.svg')}}}" alt="{{ Auth::user()->name }}">
    <div>
      <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
      <!-- <p class="app-sidebar__user-designation">
        @if(Auth::user()->role == '1')
          <span class="panel-role badge badge-info">Site Administrator</span>
        @else
           <span class="panel-role badge badge-danger">Subscriber</span>
        @endif
      </p> -->
    </div>
  </div>
  <ul class="app-menu">
    <li><a class="app-menu__item" href="/admin/home"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon icontmm-bus"></i><span class="app-menu__label">Bus Operator</span></a></li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon icontmm-bus"></i><span class="app-menu__label">Bus Type</span></a></li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon icontmm-city"></i><span class="app-menu__label">City List</span></a></li>
    <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon lnr lnr-layers"></i><span class="app-menu__label">Testimonials</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href="/listtestimonial"><i class="icon lnr lnr-list"></i> Testimonial List</a></li>
        <li><a class="treeview-item" href="/addtestimonial"  rel="noopener"><i class="icon lnr lnr-plus-circle"></i> Add Testimonial</a></li>
      </ul>
    </li>
    <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon lnr lnr-question-circle"></i><span class="app-menu__label">FAQs</span><i class="treeview-indicator fa fa-angle-right"></i></a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href="/listexpert"><i class="icon lnr lnr-list"></i> List FAQs</a></li>
        <li><a class="treeview-item" href="/addexpert"  rel="noopener"><i class="icon lnr lnr-plus-circle"></i> Add FAQs</a></li>
      </ul>
    </li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon lnr lnr-users"></i><span class="app-menu__label">Users</span></a></li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon lnr lnr-list"></i><span class="app-menu__label">TMM Leads</span></a></li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon lnr lnr-chart-bars"></i><span class="app-menu__label">Site Visitor</span></a></li>
    <li><a class="app-menu__item" href=""><i class="app-menu__icon lnr lnr-mic"></i><span class="app-menu__label">SMS Promotion</span></a></li>
  </ul>
</aside>

<nav class="navbar navbar-transparent fixed-top @if(\Request::is('searchresult'))
navbar-theme
@endif
">
  <div class="container"> 
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="/"><img src="{{{ URL::asset('img/ticketmeme.png')}}}" class="header-pages__logo" alt="logo" /></a>
      <a href="{{route('sellTicket')}}" class="btn btn-primary btn-home-cta hide-mobile">Sell Transferable Bus Ticket</a>
      <div class="btn-group btn-user-pos">
        <button id="tmm-user-drop-down-btn" data-toggle="dropdown">
          <span class="tmm-user icontmm-profile-new-unsigned"></span>
          <span class="tmm-dd-marker icontmm-down"></span>
        </button>
        <div class="dropdown-menu dropdown-menu-right" ng-controller="mainController">
          <button class="dropdown-item show-mobile" type="button" ng-click="navigateSellticket();"><i class="icon icon-arrows-right-double"></i> Sell Transferable Bus Ticket</button>
          @if (Auth::guest())                
          <a class="dropdown-item"  href="{{ route('login') }}"><i class="icon icon-arrows-right-double"></i> Sign In</a>
          <a class="dropdown-item"  href="{{ route('register') }}"><i class="icon icon-arrows-right-double"></i> Sign Up</a>

          @else
          <span class="dropdown-item"  disabled>Welcome  : <strong>{{ Auth::user()->name }}</strong></span>
          <a class="dropdown-item"  href="/home"><i class="icon icon-arrows-right-double"></i> Dashboard</a>
          <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
              <i class="icon icon-arrows-right-double"></i> Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
          @endif
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#feedback"><i class="icon icon-arrows-right-double"></i> Feedback</a>
          
        </div>
      </div>
    </div>
</nav>
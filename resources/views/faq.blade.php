@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="heading-faq">
        To Questions our users Ask
    </h2>

    <div class="faq-wrapper">
        <ul>
            @foreach($faqs as $faq)
            <li>
                <input type="checkbox" checked>
                <i></i>
                <h2>{{$faq->question}}</h2>
                <p>{{$faq->answer}}</p>
            </li>
            @endforeach
        </ul>
    </div>
</div>
    
@endsection
@extends('layouts.app')

@section('content')
<div class="seach-header-wrapper">
    <div class="search-result-page ">
        <div class="container u-no-left-right-padding">
        <section class="section-about section-about--pages">
            <div class="row">
                <div class="col-md-12 tm-search-header-selection">
                    <div class="selected-source">
                        <div class="clearfix search-wrap">
                            <div id="search" style="color:#666">
                                <form action="{{url('/searchresult')}}" id="search_form" method="post">
                                    {{csrf_field()}}
                                    <div class="clearfix search-wrap">
                                        <div class="fl search-box clearfix">
                                            <span class="fl icontmm-city icontmm">
                                            </span>
                                            <div>
                                                <div style="width: 180px;position: absolute;bottom: 0;right: 11px;">
                                                    <input name="source_id" value="{{$source}}" class="form-control" id="source" required>
                                                </div>
                                                <label for="source" class="db move-up search-label-position" >FROM</label>
                                                <div class="error-message-fixed "></div>
                                            </div>
                                        </div>
                                        <span class="icontmm-doublearrow icon" id="togglebtn" onclick="swipeCity();"></span>
                                        <div class="fl search-box">
                                            <span class="fl icontmm-city icontmm icontmm-city--position">
                                            </span>
                                            <div>
                                                <div style="width: 180px;position: absolute;bottom: 0;right: 0px;">
                                                    <input name="destination_id" value="{{$destination}}" class="form-control" id="destination" required>
                                                </div>
                                                <label for="destination"  class="db move-up search-label-position">TO</label>
                                                <div class="error-message-fixed "> </div>
                                            </div>
                                        </div>
                                        <div class="fl search-box date-box gtm-onwardCalendar u-p-relative">
                                            <span class="fl icontmm-calendar_icontmm-new icontmm">
                                            </span>
                                            <div>
                                                <div id="datepicker" class="input-group date" style="width: 133px;height: 24px;
                                                position: absolute;right: 1px;bottom: 1px;" data-date-format="yyyy-mm-dd">
                                                    <input class="form-control" name="journey-date" type="text" readonly style="    width: 100px;
                                                    height: 24px;"/>
                                                    <span class="input-group-addon" style="font-size:17px"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <label for="onward_cal" class="db text-trans-uc move-up">Onward Date</label>
                                            </div>
                                        </div>
                                        <button id="search_btn">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="show-mobile">
                            <button class="btn btn-primary btn-filter-close-mobile"><i class="icon icon-arrows-circle-up"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    </div>

    <div class="search-result-filter">
        <div class="container u-no-left-right-padding">
            <ul class="nav nav-pills nav-justified" role="tablist">
                <li class="nav-item">

                <span class="nav-link " data-toggle="tab" href="#bus-ticket" role="tab">
                    <div class="tab-icon"><div class="bus-ticket"></div></div>
                    <span class="hide-mobile">Book Bus Tickets</span>
                </span>
                </li>
                <li class="nav-item">
                <span class="nav-link active" data-toggle="tab" href="#transferable-ticket" role="tab">
                    <div class="tab-icon"><div class="transfer-ticket"></div></div>
                    <span class="hide-mobile">Transferable Bus Ticket</span>
                </span>
                </li>
                <li class="nav-item">
                <span class="nav-link" data-toggle="tab" href="#go-by-car" role="tab">
                    <div class="tab-icon"><div class="go-car"></div></div>
                    <span class="hide-mobile">Go by Shared Car</span>
                </span>
                </li>
            </ul>
            <!-- <span class="static-label">Filter By : </span><span class="show-mobile mobile-header-filter" data-toggle="modal" data-target="#show-filter-popup"><i class="fa fa-filter" aria-hidden="true"></i></span>
            <span class="hide-mobile">
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <span> Operator</span>
                <span class="filter-icon"><i class="fa fa-filter" aria-hidden="true"></i> </span>
            </button>

            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <span> Bus Types</span>
                <span class="filter-icon"><i class="fa fa-filter" aria-hidden="true"></i> </span>
            </button>

            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <span> Departure Time</span>
                <span class="filter-icon"><i class="fa fa-filter" aria-hidden="true"></i> </span>
            </button>

            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <span> Amenities</span>
                <span class="filter-icon"><i class="fa fa-filter" aria-hidden="true"></i> </span>
            </button>

            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                <span> Boarding Point</span>
                <span class="filter-icon"><i class="fa fa-filter" aria-hidden="true"></i> </span>
            </button>
            </span>

            <div class="btn-group tm-filter-by-btn tmm-sortby-dropdown ">
            <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sort by
            </button>
            <div class="dropdown-menu dropdown-menu-right">

                <button class="dropdown-item dropdown-item-form" type="button"><i class="fa fa-inr" aria-hidden="true"></i>Fare</button>
                <button class="dropdown-item dropdown-item-form" type="button"><i class="fa fa-clock-o" aria-hidden="true"></i>Departure</button>
                <button class="dropdown-item dropdown-item-form" type="button"><i class="fa fa-star-half-o" aria-hidden="true"></i>Ratings</button>
            </div>
            </div> -->
        </div>
        <!-- <div>
            <div class="collapse" id="collapseFilter">
                <div class="container">
                    <div class="row">
                    <div class="col-xs-12 col-md-2">
                        <div class="filter-list-wrapper">
                        <div class="filter-heading">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="OPERATOR" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Aditya Travels</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-2">
                        <div class="filter-list-wrapper">
                        <div class="filter-heading">
                            <label>Bus types</label>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">AC Sleeper</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Non-AC Sleeper</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">AC Seater</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Non-AC Seater</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Cab</span>
                                    </label>
                                </li>
                        </ul>
                        </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-2">
                        <div class="filter-list-wrapper">
                        <div class="filter-heading">
                            <label>departure time</label>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">00:00 to 05:59</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">06:00 to 11:59</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">12:00 to 17:59</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">18:00 to 23:59</span>
                                    </label>
                                </li>
                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-2">
                        <div class="filter-list-wrapper">
                        <div class="filter-heading">
                            <label>Aminities</label>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">WIFI</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Newspaper</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Headsets</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Water Bottle</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Blankets</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Snacks</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Charging Point</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Movie</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Reading Light</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Pillow</span>
                                    </label>
                                </li>

                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-2">
                        <div class="filter-list-wrapper">
                        <div class="filter-heading">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="BOARDING POINT" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>

                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-2">
                        <div class="filter-list-wrapper">
                        <div class="filter-heading">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" placeholder="DROPPING POINT" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="filter-content">
                            <ul>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Silk Board</span>
                                    </label>
                                </li>

                            </ul>
                            </div>
                        </div>
                    </div>

                    </div>

                    <div class="row">
                    <div class="col-xs-12 tmm-centered-btn">

                        <button class="btn btn-link">RESET ALL</button>
                        <button class="btn btn-secondary">DONE</button>
                        <button class="btn btn-secondary-dark" data-toggle="collapse" data-target="#collapseFilter">CLOSE</button>
                    </div>
                    </div>

                </div>
                </div>
        </div> -->
    </div>
</div>

<div class="search-result-container">
    <div class="u-no-left-right-padding">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane" id="bus-ticket" role="tabpanel">
            </div>
            <div class="tab-pane active" id="transferable-ticket" role="tabpanel">
            </div>
            <div class="tab-pane" id="go-by-car" role="tabpanel">

            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_script')
<div class="lds-spinner" style="width:100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
<script>
    (function () {
        $("#datepicker").datepicker({
                autoclose: true,
                todayHighlight: true
        }).datepicker('update', new Date("{{$jdate}}"));
        $('#source').autocomplete({
            serviceUrl: '/search-city',
            'autoSelectFirst': true
        });
        $('#destination').autocomplete({
            serviceUrl: '/search-city',
            'autoSelectFirst': true
        });
        var source = "{{$source}}";
        var destination = "{{$destination}}";
        var jdate = '{{$jdate}}';

        $.ajax({
            dataType: 'json',
            data: {'source':source,'destination':destination,'jdate':jdate},
            url: '/loadresult',
            success:function(data){
                $('.lds-spinner').remove();
                $('#bus-ticket').html(data.bus);
                $('#transferable-ticket').html(data.ticket);
                $('#go-by-car').html(data.car);
            }
        });
    })();
    function swipeCity()
    {
        var source = $('#source').val();
        var destination = $('#destination').val();
        $('#destination').val(source);
        $('#source').val(destination);
    }
    function getSeatLayout(source,destination,jdate,inventoryType,routeScheduleId){
        if($('#seatlayout-'+routeScheduleId).closest('.seatcontent').hasClass('show'))
            return;
        $.ajax({
            dataType: 'json',
            data: {'source':source,'destination':destination,'jdate':jdate,'inventoryType':inventoryType,'routeScheduleId':routeScheduleId},
            url: '/getSeatLayout',
            success:function(data){
                //data = JSON.parse(data);
                $('#seatlayout-'+routeScheduleId).html(data['result']);
            }
        });
    }
    function setTicketId(id)
    {
        $('#ticket_id').val(id);
    }
</script>
@endsection

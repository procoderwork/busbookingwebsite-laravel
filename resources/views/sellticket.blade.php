@extends('layouts.app')

@section('content') 

<div class="container u-no-left-right-padding" ng-controller="mainController">
    <div class="fluid-form-wrapper">
        <div class="card">
            <div class="card-header">
            Sell Transferable Bus Ticket
            </div>
            <div class="card-block tm-form-background">
            <form action="{{route('post_sellTicket')}}" id="search_form" onsubmit="onSubmit()" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                        <label for="source-from">From <small>(Required)</small> :</label>
                        <input name="source_id" class="form-control" id="source-from" placeholder="Source" required>
                        </div> 
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                        <label for="journey-destination">To <small>(Required)</small> :</label>
                        <input name="destination_id" class="form-control" id="journey-destination" placeholder="Destination" required>
                        </div> 
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <label for="journey-date">Journey Date &amp; Time <small>(Required)</small> : </label>
                        <div class="form-group">
                            <div class="input-group date" id="datepicker" data-target-input="nearest">
                                <input name="journey_time" type="text" id="journey-date" required class="form-control datetimepicker-input" data-target="#datepicker"/>
                                <div class="input-group-append" data-target="#datepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>


                    <div class="row">
                    <div class="col-xs-12 col-md-4 form-component-spacer">
                        <div class="form-group">
                            <label for="bus-operator">Bus Operator <small>(Required)</small> :</label>
                            <select class="custom-select" id="bus-operator" name="bus_operator_id" required>
                                <option selected value="">Bus Operator</option>
                                @foreach($bus_operators as $operator)
                                <option value="{{$operator->id}}">{{$operator->name}}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>

                    <div class="col-xs-12 col-md-4 form-component-spacer">
                        <label for="journey-bearth">Bus Type <small>(Required)</small> :</label><br>

                        <select class="custom-select" id="journey-bearth" name="bus_type_id" required>
                            <option selected value="">Bus Type</option>
                            @foreach($bus_types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="col-xs-12 col-md-4 form-component-spacer">
                        <div class="form-group">
                            <label for="offer-price">Price (INR) <small>(Required)</small> :</label>
                            <input type="text" name="price" class="form-control" placeholder="Ticket Selling Price" id="offer-price" required>
                        </div> 
                    </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-4 form-component-spacer">
                            <div class="form-group">
                            <label for="seller-name">Name <small>(Required)</small> :</label>
                            <input type="text" name="name" class="form-control" @if(Auth::check())
                            value="{{Auth::user()->name}}" readonly
                            @endif placeholder="Your Name" id="seller-name" required>
                            </div> 
                        </div>
                        <div class="col-xs-12 col-md-4 form-component-spacer">
                            <div class="form-group">
                            <label for="seller-name">Contact Number <small>(Required)</small> :</label>
                            <input type="text" name="mobile" @if(Auth::check())
                            value="{{Auth::user()->mobile}}" readonly
                            @endif class="form-control" placeholder="Contact Number" id="seller-name" required>
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-xs-12 col-md-12 form-component-spacer">
                        <div class="form-group">
                            <label for="comment">Ticket Details <small>(Required)</small>:
                            <div class="tm-help-tip">
                                <p>
                                    Please enter complete Ticket Details <span class="u-c-orange">(No of Seats, Seat Gender, Birth Details etc.)</span>
                                </p>
                                </div>
                            </label>
                            <textarea class="form-control" name="detail" rows="5" id="comment" placeholder="Enter No. of Seats, Seat Gender, Birth Details (eg. 4 Seats, 2 Male 2 Female, Middle-2, Lower-2)" required></textarea>
                        </div>
                    </div>
                    </div>

                    <div class="row">
                    <div class="tmm-centered-btn col-xs-12">
                        <button class="btn btn-primary btn-tmm">Post your Ticket</button>
                        <button onclick="event.preventDefault();document.location.href='{{url('/')}}'" class="btn btn-link btn-tmm">Cancel</button>
                    </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_script')
<script>
    var b_source_selected = false;
    var b_destination_selected = false;
    (function () {
        $("#datepicker").datetimepicker({
            defaultDate:new Date() 
        });
        $('#source-from').autocomplete({
            serviceUrl: '/search-city',
            'autoSelectFirst': true,
            onSelect: function (suggestion) {
                b_source_selected = true;
            },
            onInvalidateSelection: function () {
                b_source_selected = false;
            }
        });
        $('#journey-destination').autocomplete({
            serviceUrl: '/search-city',
            'autoSelectFirst': true,
            onSelect: function (suggestion) {
                b_destination_selected = true;
            },
            onInvalidateSelection: function () {
                b_destination_selected = false;
            }
        });
    })();
    function onSubmit()
    {
        if(b_source_selected && b_destination_selected)
            return true;
        else{
           event.preventDefault();
            swal("Alert!",'Select Correct Places', "error"); 
        } 
    }
</script>
@endsection
@extends('layouts.adminauth')

@section('content')
<section class="material-half-bg">
   <div class="cover"></div>
 </section>
 <section class="login-content" >
    <div class="logo">
     <a class="navbar-brand" href="/"><img src="{{{ URL::asset('img/ticketmeme.png')}}}" class="auth-brand-logo" alt="Ticket Me me logo" /></a>
    </div>
    <div class="login-box" id="userauth">
     <div>
         <h3 class="login-head"><i class="lnr lnr-user"></i> User Login</h3>
     </div>
     <div class="row">
         <div class="col-xs-12 col-md-6">
            <a href="login/facebook" class="btn btn-block btn-primary btn-block">Login with Facebook</a>
            <a href="login/google" class="btn btn-block btn-primary btn-block">Login with Google</a>
            <a href="login/twitter" class="btn btn-block btn-primary btn-block">Login with Twitter</a>
            <a href="login/linkedin" class="btn btn-block btn-primary btn-block">Login with Linkedin</a>
         </div>
         <div class="col-xs-12 col-md-6">
             <form class="form-horizontal" method="POST" action="{{ route('login') }}">
               {{ csrf_field() }}
               
               <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                 <label for="mobile" class="control-label">Mobile Number</label>
                 <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus>
                 @if ($errors->has('mobile'))
                     <span class="help-block">
                         <strong>{{ $errors->first('mobile') }}</strong>
                     </span>
                 @endif
               </div>
               <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                 <label for="password" class="control-label">Password</label>
                 <input id="password" type="password" class="form-control" name="password" required>
               </div>
               <div class="form-group">
                 <div class="utility">
                   <div class="animated-checkbox">
                      <label>
                        <input input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><span class="label-text">Stay Signed in</span>
                      </label>
                   </div>
                 </div>
               </div>
               <div class="form-group btn-container">
                 <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
               </div>
             </form>
         </div>
     </div>
     <div class="form-group btn-container">
         <p class="semibold-text mb-2 text-center m-t-small">
            <a class="tmm-home-link" href="{{ route('resetPassword') }}">
                Forgot Your Password?
            </a>
            <a class="tmm-home-link" href="{{ route('register') }}">
                Not an User yet?
            </a>
        </p>
     </div>
    </div>
</section>

@endsection

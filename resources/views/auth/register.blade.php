@extends('layouts.adminauth')

@section('content')

<section class="material-half-bg">
   <div class="cover"></div>
 </section>
 <section class="login-content" >
    <div class="logo">
     <a class="navbar-brand" href="/"><img src="{{{ URL::asset('img/ticketmeme.png')}}}" class="auth-brand-logo" alt="Ticket Me me logo" /></a>
    </div>
    <div class="login-box" id="userauth">
     <div>
         <h3 class="login-head"><i class="lnr lnr-user"></i> New User Registration</h3>
     </div>
     <div class="row">
         <div class="col-xs-12 col-md-12">
             <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Name</label>

                            <div>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="control-label">Mobile Number</label>

                            <div>
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <div>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-primary btn-block">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
         </div>
     </div>
     <div class="form-group btn-container">
         <p class="semibold-text mb-2 text-center m-t-small">
            <a class="tmm-home-link" href="{{ route('login') }}">
                Already an User?
            </a>
            <a class="tmm-home-link" href="/">
                Back to Home
            </a>
        </p>
     </div>
    </div>
</section>

@endsection

var app = angular.module("ticketmeme", ["ngRoute", "cuppaDatepickerDirective"]);

app.controller("mainController", function($scope, $rootScope, $location) {

    $scope.location = $location.path();
    $rootScope.$on('$routeChangeSuccess', function() {
        $scope.location = $location.path();
    });

    $scope.source = 'Bangalore';
    $scope.destination = 'Hydrabad';

    $scope.onwardDate = new Date();

    $scope.navigateSellticket = function(){
        $location.path("/sellTicket" );
    }


    $scope.navigateDashboard = function(){
        $location.path("/dashboard" );
    }

    
    $scope.modifySearch = function(){
        $scope.isModify = true;
        $scope.source = 'Bangalore';
        $scope.destination = 'Gangawati';
        $scope.journeydate = now();
    }

    $scope.closemodifySearch = function(){
        $scope.isModify = false;
    }

    $scope.scrollToTop = function(){
        $('html, body').animate({scrollTop : 0},900);
    }

    $scope.swipeCity = function(){
        var source = $scope.source;
        var destination = $scope.destination;

        $scope.source = destination;
        $scope.destination = source;

    }

    $scope.menuItems = ['My Profile', 'Ticket History', 'Change Password'];
    $scope.activeMenu = $scope.menuItems[0];

    $scope.setActive = function(menuItem) {
        $scope.activeMenu = menuItem
    }

});


app.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
});

app.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
      
        angular.element($window).bind("scroll", function() {
            if (this.pageYOffset >= 100) {
                 scope.boolChangeClass = true;
                 console.log('Scrolled below header.');
             } else {
                 scope.boolChangeClass = false;
                 console.log('Header is in view.');
             }
            scope.$apply();
        });
    };
});
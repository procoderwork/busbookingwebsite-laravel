<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Controller@index');
Route::get('/privacy', 'Controller@privacy');
Route::get('/termsofuse', 'Controller@terms');
Route::get('/faq', 'Controller@faq');
Route::post('/searchresult', 'Controller@find')->name('searchresult');
Route::get('/loadresult','Controller@ajaxPageLoading')->name('loadresult');
Route::get('/getSeatLayout','Controller@getBusLayout');

Auth::routes();

Route::get('/resetPassword','Auth\ForgotPasswordController@showResetUI')->name('resetPassword');
Route::post('/resetPassword','Auth\ForgotPasswordController@sendResetedPassword');

Route::get('/home', 'HomeController@index')->name('dashboard');
Route::post('/update-profile','HomeController@updateProfile')->name('post_update_profile');
Route::post('/update-password','HomeController@updatePassword')->name('post_update_password');

Route::get('sell-ticket','Controller@sellTicket')->name('sellTicket');
Route::post('sell-ticket','Controller@addTicket')->name('post_sellTicket');
Route::get('/search-city','Controller@searchCity')->name('search-city');
Route::get('/search-city1','Controller@searchCity1')->name('search-city1');
Route::post('/subscribe','Controller@subscribe')->name('subscribe');
Route::post('/feedback','Controller@feedback')->name('feedback');
Route::post('/request-ticket','Controller@requestTicket')->name('request-ticket');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('admin/', 'Admin\LoginController@showLogin');
Route::post('admin/login', 'Admin\LoginController@login')->name('admin.login');

Route::middleware(['admin'])->prefix('admin')->name('admin.')->group(function () {
    //User Settings
    Route::get('/home', 'Admin\AdminController@index')->name('home');
    Route::post('/update_name_email','Admin\AdminController@updateUsername_Email')->name('update.name.email');
    Route::post('/update_password','Admin\AdminController@updatePassword')->name('update.password');
    Route::get('/logout', 'Admin\LoginController@logout')->name('logout');

    //Ticket Resource
    Route::resource('ticket','Admin\TicketController');
    Route::post('ticket/update_status','Admin\TicketController@updateStatus')->name('ticket.update_status');

    Route::get('user/','Admin\UserController@index')->name('user.index');
    Route::post('user/update_status','Admin\UserController@updateStatus')->name('user.update_status');

    Route::get('feedback/','Admin\FeedbackController@index')->name('feedback.index');
    Route::get('feedback/{feedback}/show','Admin\FeedbackController@show')->name('feedback.show');
    Route::get('feedback/{feedback}/destroy','Admin\FeedbackController@destroy')->name('feedback.destroy');

    Route::get('faq','Admin\FaqController@index')->name('faq.index');
    Route::get('faq/{faq}/destroy','Admin\FaqController@destroy')->name('faq.destroy');
    Route::get('faq/{faq}/edit','Admin\FaqController@edit')->name('faq.edit');
    Route::post('faq/{faq}/update','Admin\FaqController@update')->name('faq.update');
    Route::post('faq/insert','Admin\FaqController@insert')->name('faq.insert');
    Route::get('faq/add','Admin\FaqController@add')->name('faq.add');

    Route::get('subscribe','Admin\SubscribeController@index')->name('subscribe.index');
    Route::get('subscribe/{subscribe}.destroy','Admin\SubscribeController@destroy')->name('subscribe.destroy');

    Route::get('bustype' , 'Admin\BusTypeController@index')->name('bustype.index');
    Route::post('bustype/add' , 'Admin\BusTypeController@insert')->name('bustype.insert');
    Route::get('bustype/{bustype}/destroy','Admin\BusTypeController@destroy')->name('bustype.destroy');
    Route::get('bustype/{bustype}/edit','Admin\BusTypeController@edit')->name('bustype.edit');
    Route::post('bustype/{bustype}/update','Admin\BusTypeController@update')->name('bustype.update');

    Route::get('busoperator' , 'Admin\BusOperatorController@index')->name('busoperator.index');
    Route::post('busoperator/add' , 'Admin\BusOperatorController@insert')->name('busoperator.insert');
    Route::get('busoperator/{busoperator}/destroy','Admin\BusOperatorController@destroy')->name('busoperator.destroy');
    Route::get('busoperator/{busoperator}/edit','Admin\BusOperatorController@edit')->name('busoperator.edit');
    Route::post('busoperator/{busoperator}/update','Admin\BusOperatorController@update')->name('busoperator.update');
});

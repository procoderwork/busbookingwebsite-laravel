$(document).ready(function(){
var owl = $(".touch-slider");
    owl.owlCarousel({
      navigation: false,
      pagination: true,
      slideSpeed: 1000,
      stopOnHover: true,
      autoPlay: true,
      items: 2,
      itemsDesktop : [1199,2],
      itemsDesktopSmall: [1024, 2],
      itemsTablet: [600, 1],
      itemsMobile: [479, 1]
    });
        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,      
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(event) {
            event.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });

        $('.bhoechie-tab-content .btn-outline-primary').click(function(){
            var $this = $(this);
            $this.toggleClass('SeeMore2');
            if($this.hasClass('SeeMore2')){
                $this.text('Less Details');         
            } else {
                $this.text('More Details');
            }
        });

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        $(".navbar-brand-post-header").hide();
        if (scroll >= 100) {
           $(".search-result-page").addClass("search-fix-top-scroll", 2000);
           $(".search-result-filter").addClass("tmm-fixed-tab", 2000);
           $(".navbar").addClass("navbar-theme", 2000);
        }
        if (scroll < 100) {
           $(".search-result-page").removeClass("search-fix-top-scroll", 2000);
           $(".search-result-filter").removeClass("tmm-fixed-tab", 2000);
            
        }
    });


    $('.nav-link').click(function(e){
        e.preventDefault();
    });


    $(".dropdown-item-form").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
      });

      $('.filter-content').mCustomScrollbar({
        theme:"3d"
    });

    $('.bhoechie-tab-content').mCustomScrollbar({
        theme:"3d"
    });
    

  /*  $(function() {
        $('.search-result-container').click(function (event) {
            $('.collapse').collapse('hide');
        });
    });
*/
    

    

    
    $('.u-m-right-medium').hide();
    $('.tmm-modify-search-cta').hide();
    $('#modify-search').click(function(){
        var $selector = $('.journey-places');
        if ($selector.attr('contenteditable')) {
            $selector.removeAttr('contenteditable');
            $('.u-m-right-medium').hide();
            $('.tm-search-header-selection').css('margin-top', '.5rem');
            $('.tmm-modify-search-cta').hide();
        } else {
            $selector.attr('contenteditable', true);
            $('.u-m-right-medium').show();
            $('.tm-search-header-selection').css('margin-top', '-.2rem');
            $('.tmm-modify-search-cta').show();
        }
    });

    $('.list-toggle').click(function() {
        var $listSort = $('.list-sort');
        if ($listSort.attr('colspan')) {
            $listSort.removeAttr('colspan');
        } else {
            $listSort.attr('colspan', 6);
        }
    });

    $('#go-by-car .btn-outline-primary').click(function(){
        var $this = $(this);
        $this.toggleClass('SeeMore2');
        if($this.hasClass('SeeMore2')){
            $this.text('Less Details');         
        } else {
            $this.text('More Details');
        }
    });

    $('#modify-search').click(function(){
        var $this = $(this);
        $this.toggleClass('SeeMore2');
        if($this.hasClass('SeeMore2')){
            $this.text('CLOSE');         
        } else {
            $this.text('MODIFY');
        }
    });

    $('.btn-tm-search').click(function(){
        var $this = $(this);
        $this.toggleClass('progress-bar-striped progress-bar-animated');
    });

    $('.tm-offer-wrapper li')
        .on('mouseenter', function(){
            var div = $(this);
            div.siblings().addClass( "hide");
            div.addClass("expanded-offer");
            $('.content').css('display', 'block');
        })
        .on('mouseleave', function(){
            var div = $(this);
            div.siblings().removeClass( "hide");
            div.removeClass("expanded-offer");
            $('.content').css('display', 'none');
        });
});
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '2019021255012408',
        'client_secret' => 'f3b22ce3467c07c0b2fb738c11c5c6de',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'HBnhULzy3cllMmrGd8tFGj4fR', 
        'client_secret' => 'kj5N1p5PFDfQVm4IG4U3ISJavWW4blr8gC5bBSbCndKIAxno7D',
        'redirect' => 'http://localhost:8000/login/twitter/callback',
    ],

    'google' => [
        'client_id' => '711172047135-iplidilu3tgeglfjs1f02qgh8o5a1c2a.apps.googleusercontent.com',
        'client_secret' => '9Uw_yOrLAzBE0pcABNFGnrEN',
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],
    'linkedin' => [
        'client_id' => '817r38lw3yrunc',
        'client_secret' => 'Exwu0r0MhBMUgwyy',
        'redirect' => 'http://localhost:8000/login/linkedin/callback'
    ],

];

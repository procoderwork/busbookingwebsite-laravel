<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $guarded = ['id'];
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function destination()
    {
        return $this->belongsTo('App\City','destination_id');
    }
    public function source()
    {
        return $this->belongsTo('App\City','source_id');
    }
    public function bus_operator()
    {
        return $this->belongsTo('App\BusOperator');
    }
    public function bus_type()
    {
        return $this->belongsTo('App\BusType');
    }
}

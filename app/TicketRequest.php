<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketRequest extends Model
{
    //
    protected $guarded = ['id'];
    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }
}

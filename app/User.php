<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mobile','email', 'password','is_enabled'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function socialProvider()
    {
        return $this->hasMany(SocialProvider::class);
    }

    function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
    public function request_tickets()
    {
        return $this->hasMany('App\TicketRequest');
    }
}

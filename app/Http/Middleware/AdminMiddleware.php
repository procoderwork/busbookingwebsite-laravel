<?php

namespace App\Http\Middleware;

use Closure;
use App\Setting;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('is_admin'))
        {
          if(session('is_admin')=='true')
          {
            $email_setting = Setting::where('name','admin_email')->get();
            $password_setting = Setting::where('name','admin_password')->get();
            $admin_setting = Setting::where('name','admin_username')->get();
            if(count($email_setting)&&count($password_setting)&&count($admin_setting))
            {
              if(session('admin_email')==$email_setting[0]->value && session('admin_password')==$password_setting[0]->value && session('admin_username')==$admin_setting[0]->value)
              {
                return $next($request);
              }
            }
          }
        }
        return redirect('/admin');
    }
}

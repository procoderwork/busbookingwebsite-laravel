<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Support\Facades\Hash;
class LoginController extends Controller
{

    public function showLogin()
    {
      if(session()->has('is_admin'))
        if(session('is_admin')=='true')
          return redirect('admin/home');
      return view('admin.login');
    }
    public function login(Request $request)
    {
      if(session()->has('is_admin'))
        if(session('is_admin')=='true')
          return redirect('admin/home');
      $email = $request->get('email');
      $password = $request->get('password');
      $email_setting = Setting::where('name','admin_email')->get();
      $password_setting = Setting::where('name','admin_password')->get();
      $username_setting = Setting::where('name','admin_username')->get();
      if(count($email_setting)&&count($password_setting)&&count($username_setting))
      {
        if($email == $email_setting[0]->value && Hash::check($password , $password_setting[0]->value)){
          $this->setSession($email_setting[0]->value,$password_setting[0]->value,$username_setting[0]->value);
          return redirect('admin/home');
        }
        else {
          return redirect()->back();
        }
      }
      else {
        if(count($email_setting))
          $email_setting[0]->destroy();
        if(count($password_setting))
          $password_setting[0]->destroy();
        if(count($username_setting))
          $username_setting[0]->destroy();
        if($email == 'admin@ticketmeme.com' && $password == '123456789')
        {
          $email_setting = Setting::create(['name'=>'admin_email','value'=>'admin@ticketmeme.com']);
          $password_setting = Setting::create(['name'=>'admin_password','value'=>bcrypt('123456789')]);
          $username_setting = Setting::create(['name'=>'admin_username','value'=>'Admin']);
          $this->setSession($email_setting->value,$password_setting->value,$username_setting->value);
        }
        else
          return redirect()->back();
      }
    }
    public function logout()
    {
      session()->forget('is_admin');
      session()->forget('admin_email');
      session()->forget('admin_password');
      session()->forget('admin_username');
      return redirect('admin/');
    }
    public function setSession($email , $password , $username)
    {
      session(['admin_email'=>$email,'is_admin'=>'true','admin_password'=>$password,'admin_username'=>$username]);
    }
}

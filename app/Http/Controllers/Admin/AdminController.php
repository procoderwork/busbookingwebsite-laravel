<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use Hash;
class AdminController extends Controller
{
    public function index()
    {
        return view('admin.home');
    }
    public function updateUsername_Email(Request $request)
    {
        $username = $request->get('username');
        $email = $request->get('email');
        Setting::where('name','admin_username')->first()->update(['value'=>$username]);
        Setting::where('name','admin_email')->first()->update(['value'=>$email]);
        session(['admin_email'=>$email,'admin_username'=>$username]);
        return redirect()->back();
    }
    public function updatePassword(Request $request)
    {
      $current_password = $request->get('current_password');
      $password_setting = Setting::where('name','admin_password')->first();
      if(!Hash::check($current_password , $password_setting->value))
        return redirect()->back()->with('current_password','Your current password is not match');
      $this->validate($request,['new_password'=>'confirmed']);

      $password_setting->value = bcrypt($request->get('new_password'));
      $password_setting->save();
      session(['admin_password'=>$password_setting->value]);
      return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;
class FeedbackController extends Controller
{
    //
    public function index()
    {
      $feedbacks = Feedback::all();
      return view('admin.feedback.index',compact('feedbacks'));
    }
    public function show(Feedback $feedback)
    {
      return view('admin.feedback.show',compact('feedback'));
    }
    public function destroy(Feedback $feedback)
    {
      Feedback::destroy($feedback->id);
      return redirect()->back();
    }
}

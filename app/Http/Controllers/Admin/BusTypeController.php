<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusType;
class BusTypeController extends Controller
{
    //
    public function index()
    {
    	$bustypes = BusType::all();
    	return view('admin.bustype.index',compact('bustypes'));
    }
    public function insert(Request $request)
    {
    	BusType::create(['name'=>$request->get('name')]);
    	return redirect()->back();
    }
    public function destroy(BusType $bustype)
    {
    	BusType::destroy($bustype->id);
    	return redirect()->back();
    }
    public function edit(BusType $bustype)
    {
    	return view('admin.bustype.edit',compact('bustype'));
    }
    public function update(Request $request, BusType $bustype)
    {
    	$bustype->update(['name'=>$request->get('name')]);
    	return redirect()->back();
    }
}

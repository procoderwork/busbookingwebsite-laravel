<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
class FaqController extends Controller
{
    //
    function index()
    {
    	$faqs = Faq::all();
    	return view('admin.faq.index',compact('faqs'));
    }
    function destroy(Faq $faq)
    {
    	Faq::destroy($faq->id);
    	return redirect()->back();
    }
    function insert(Request $request)
    {
        Faq::create($request->all());
    	return redirect('admin/faq');
    }
    function add()
    {
    	return view('admin.faq.add');
    }
    function update(Request $request , Faq $faq)
    {
        $faq->update($request->all());
    	return redirect()->back();
    }
    function edit(Faq $faq)
    {
		return view('admin.faq.edit',compact('faq'));
    }
}

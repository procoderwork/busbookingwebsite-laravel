<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscribe;
class SubscribeController extends Controller
{
    //
    public function index()
    {
    	$subscribes = Subscribe::all();
      return view('admin.subscribe.index',compact('subscribes'));
    }
    public function destroy(Subscribe $subscribe)
    {
    	Subscribe::destroy($subscribe->id);
    	return redirect()->back();
    }
}

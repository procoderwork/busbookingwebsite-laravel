<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusOperator;
class BusOperatorController extends Controller
{
    //
    public function index()
    {
    	$busoperators = BusOperator::all();
    	return view('admin.busoperator.index',compact('busoperators'));
    }
    public function insert(Request $request)
    {
    	BusOperator::create(['name'=>$request->get('name')]);
    	return redirect()->back();
    }
    public function destroy(BusOperator $busoperator)
    {
    	BusOperator::destroy($busoperator->id);
    	return redirect()->back();
    }
    public function edit(BusOperator $busoperator)
    {
    	return view('admin.busoperator.edit',compact('busoperator'));
    }
    public function update(Request $request, BusOperator $busoperator)
    {
    	$busoperator->update(['name'=>$request->get('name')]);
    	return redirect()->back();
    }
}

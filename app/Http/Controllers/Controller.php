<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\City;
use App\User;
use App\Faq;
use Message\Sms\Message;
use App\BusType;
use App\Subscribe;
use App\BusOperator;
use App\Ticket;
use App\Feedback;
use App\TicketRequest;
use GuzzleHttp\Client;
use GuzzleHttp\PhpStreamRequestFactory;
use File;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
      // $json = File::get("city.json");
      // $data = json_decode($json);
      // $array1 = (array) $data;
      // foreach( $array1['stationList'] as $station)
      //    City::create(['name'=>$station->stationName]);
      return view('welcome');
    }
    public function privacy()
    {
      return view('privacy');
    }
    public function terms()
    {
      return view('terms');
    }
    public function faq()
    {
      $faqs = Faq::all();
      return view('faq',compact('faqs'));
    }

    public function find(Request $request)
    {
      $source = $request->input('source_id');
      $destination = $request->input('destination_id');
      if($source == $destination)
        return redirect('/');
      $jdate = $request->input('journey-date');
      return view('searchresult',compact('source','destination','jdate'));
    }
    public function ajaxPageLoading(Request $request)
    {
      $source = $request->input('source');
      $destination = $request->input('destination');
      if($source == $destination)
        return redirect('/');
      $jdate = $request->input('jdate');
      $source_city = City::where('name',$source)->get();
      $destination_city = City::where('name',$destination)->get();
      if(count($source_city)&&count($destination_city))
        $tickets = Ticket::where('source_id',$source)->where('destination_id',$destination)->where('status','1')->whereDate('journey_time',$jdate)->get();
      else
        $tickets = [];
      $client = new Client();
      $url = 'https://public-api.blablacar.com/api/v2/trips?fn='.$source.'&tn='.$destination.'&locale=en_IN&db='.$jdate;
      $request = $client->get($url,['headers'=>[
        'key' => 'cc5c1db4e60f44fe981ebdf67e773d88'
      ]]);
      $trips = [];
      if($request->getStatusCode() == '200')
      {
        $car_pool_api_data = json_decode($request->getBody(),true);
        foreach($car_pool_api_data['trips'] as $trip){
          $permanent_id = $trip['permanent_id'];
          $url = 'https://public-api.blablacar.com/api/v2/trips/'.$permanent_id.'?locale=en_IN';
          $req = $client->get($url,['headers'=>[
            'key' => 'cc5c1db4e60f44fe981ebdf67e773d88'
          ]]);
          if($request->getStatusCode() == '200')
            $trips[] = json_decode($req->getBody(),true); 
        }
      }
      $url = 'http://test.etravelsmart.com/etsAPI/api/getAvailableBuses?sourceCity='.$source.'&destinationCity='.$destination.'&doj='.$jdate;
      $request = $client->get($url,['auth'=>['admin@ticketmeme.com','Admin123','digest']]);
      $buses = json_decode($request->getBody(),true);
      $ticket_view = view('ajax_search_result.transferable_ticket',compact('source','destination','jdate','tickets'))->render();
      $bus_view =  view('ajax_search_result.bus_ticket',compact('source','destination','jdate','buses'))->render();
      $car_view =  view('ajax_search_result.go_by_car',compact('source','destination','jdate','trips'))->render();
      return response()->json(['bus'=>$bus_view,'ticket'=>$ticket_view,'car'=>$car_view]);
    }
    public function getBusLayout(Request $request)
    {
      $bustype = "";
      $rowarray = array();
      $array_zindez0_tr1 = array();
      $array_zindez0_tr2 = array();
      $array_zindez0_tr3 = array();
      $array_zindez0_tr4 = array();
      $array_zindez0_tr5 = array();

      $array_zindez1_tr1 = array();
      $array_zindez1_tr2 = array();
      $array_zindez1_tr3 = array();
      $array_zindez1_tr4 = array();
      $array_zindez1_tr5 = array();

      $array_row_values0 = array();
      $array_col_values0 = array();

      $array_row_values1 = array();
      $array_col_values1 = array();

      $zindex0 = false;
      $zindex1 = false;

      $source = $request->get('source');
      $destination = $request->get('destination');
      $inventoryType = $request->get('inventoryType');
      $jdate = $request->get('jdate');
      $routeScheduleId = $request->get('routeScheduleId');
      $url = 'http://test.etravelsmart.com/etsAPI/api/getBusLayout?sourceCity='.$source.'&destinationCity='.$destination.'&doj='.$jdate.'&inventoryType='.$inventoryType.'&routeScheduleId='.$routeScheduleId;
      $client = new Client();
      $request = $client->get($url,['auth'=>['admin@ticketmeme.com','Admin123','digest']]);
      $seatarray = json_decode($request->getBody());
      if(empty($seatarray->seats))
        return response()->json(['status'=>'failed']);
      $tablestr = '';
      $tr1 = '';
      $tr2 = '';
      $tr3 = '';
      $tr4 = '';
      $tr5 = '';
      $zindex0str = '';
      $zindex1str = '';
      foreach($seatarray->seats as $sr)
      {
        $width = (string)$sr->width;
        $ladiesSeat = (string)$sr->ladiesSeat;
        $fare = (string)$sr->fare;
        $zIndex = (string)$sr->zIndex;

        $serviceTaxAmount = (string)$sr->serviceTaxAmount;
        $commission = (string)$sr->commission;
        $operatorServiceChargeAbsolute = (string)$sr->operatorServiceChargeAbsolute;
        $operatorServiceChargePercent = (string)$sr->operatorServiceChargePercent;
        $totalFareWithTaxes = (string)$sr->totalFareWithTaxes;

        $bookedBy = (string)$sr->bookedBy;
        $ac = (string)$sr->ac;
        $sleeper = (string)$sr->sleeper;
        $serviceTaxPer = (string)$sr->serviceTaxPer;
        $available = (string)$sr->available;
        $column = (string)$sr->column;
        $row = (string)$sr->row;
        $length = (string)$sr->length;
        $id = (string)$sr->id;
        //echo "Width :".$width."   zIndex : ".$zIndex."   Column : ".$column."   Row : ".$row;
        if($zIndex == "0")
        {
          array_push($array_row_values0, $row);
          array_push($array_col_values0, $column);
          $zindex0 = true;
          $rowarray[0][$row][$column] = $sr;
        }
        if($zIndex == "1")
        {
          array_push($array_row_values1, $row);
          array_push($array_col_values1, $column);
          $zindex1 = true;
          $rowarray[1][$row][$column] = $sr;
        }
      }
      //dd($rowarray);
      $tablestr0 = '';
      if($zindex0 == true)
      {
        $tablestr0 .= '<table style="width:500px;height:120px;" border=0>';
        for($r0 = 0;$r0 <= max($array_row_values0);$r0++)
        {
          $tablestr0.='<tr>';
          if($r0 == 0)
          {
            $tablestr0.= '<td style="width:1.5%"><div class="seat-sprite driver-seat"></div></td>';
          }
          else
          {
            $tablestr0.=  '<td style="width:8%"><div></div></td>';
          }
          for($c0 = 0;$c0 <= max($array_col_values0);$c0++)
          {
              $class3 = '';


              if(isset($rowarray[0][$r0][$c0]))
              {
                //$tablestr0.= $rowarray[0][$r0][$c0]->row.' '.$rowarray[0][$r0][$c0]->column;
                if((string)$rowarray[0][$r0][$c0]->length == 1)
                {
                  if((string)$rowarray[0][$r0][$c0]->available == 1)
                  {
                    $class3.='seat-sprite seat-avail';
                  }
                  else
                  {
                    $class3.='seat-sprite male-seat-booked';
                  }

                }
                if((string)$rowarray[0][$r0][$c0]->length == 2)
                {
                  if((string)$rowarray[0][$r0][$c0]->available == 1)
                  {
                    $class3.='hori-sleep-sprite hori-sleep-avail';
                  }
                  else
                  {
                    $class3.='hori-sleep-sprite hori-sleep-booked';
                  }

                }

                $tablestr0.='<td colspan='.$rowarray[0][$r0][$c0]->length.'>';
                $tablestr0 .= '<div id="c'.(string)$rowarray[0][$r0][$c0]->row.'-'.(string)$rowarray[0][$r0][$c0]->column.'-0~'.(string)$routeScheduleId.'" title="Seat No. '.(string)$rowarray[0][$r0][$c0]->id.' row '.(string)$rowarray[0][$r0][$c0]->row.' col = '.(string)$rowarray[0][$r0][$c0]->column.'" class="'.$class3.'"></div>';
                $tablestr0.='</td>';
              }
              else
              {
              }
          }
          $tablestr0.='</tr>';
        }
        $tablestr0.='</table>';
      }
      $tablestr1 = '';
      if($zindex1 == true)
      {
          $tablestr1 .= '<table style="width:500px;height:120px;border-bottom:1px solid #999999;margin-bottom: 10px;" border=0>';
          for($r1 = 0;$r1 <= max($array_row_values1);$r1++)
          {
            $tablestr1.='<tr>';
            if($r1 == 0)
            {
              $tablestr1.= '<td style="width:1.5%"><div></div></td>';
            }
            else
            {
              $tablestr1.=  '<td style="width:8%"><div></div></td>';
            }
            for($c1 = 0;$c1 <= max($array_col_values1);$c1++)
            {
                $class3 = '';


                if(isset($rowarray[1][$r1][$c1]))
                {
                  //$tablestr0.= $rowarray[0][$r0][$c0]->row.' '.$rowarray[0][$r0][$c0]->column;
                  if((string)$rowarray[1][$r1][$c1]->length == 1)
                  {
                    if((string)$rowarray[1][$r1][$c1]->available == 1)
                    {
                      $class3.='seat-sprite seat-avail';
                    }
                    else
                    {
                      $class3.='seat-sprite male-seat-booked';
                    }

                  }
                  if((string)$rowarray[1][$r1][$c1]->length == 2)
                  {
                    if((string)$rowarray[1][$r1][$c1]->available == 1)
                    {
                      $class3.='hori-sleep-sprite hori-sleep-avail';
                    }
                    else
                    {
                      $class3.='hori-sleep-sprite hori-sleep-booked';
                    }

                  }
                  $tablestr1.='<td colspan='.(string)$rowarray[1][$r1][$c1]->length.'>';
                  $tablestr1 .= '<div title="Seat No. '.(string)$rowarray[1][$r1][$c1]->id.' row '.(string)$rowarray[1][$r1][$c1]->row.' col = '.(string)$rowarray[1][$r1][$c1]->column.'" class="'.$class3.'"></div>';
                  $tablestr1.='</td>';
                }
                else
                {

                }


            }
            $tablestr1.='</tr>';
          }
          $tablestr1.='</table>';

      }
      $text = '<div class="ssb-layout-box">';
        $text .= $tablestr1;
        $text .= $tablestr0;
      $text .= '</div>';
      return response()->json(['status'=>'ok','result'=>$text]);
    }

    public function searchCity(Request $request)
    {
      $keyword = $request->get('query');
      $cities = City::where('name','like','%'.$keyword.'%')->take(10)->get();
      $data = [];
      foreach($cities as $city)
      {
        $data[] = $city->name;
      }
      return response()->json(['suggestions'=>$data]);
    }
    public function searchCity1(Request $request)
    {
      $keyword = $request->get('term');
      $cities = City::where('name','like','%'.$keyword.'%')->get();
      $data = [];
      foreach($cities as $city)
      {
        $data[] = ['id'=>$city->id,'text'=>$city->name];
      }
      return response()->json(['items'=>$data]);
    }
    public function sellTicket()
    {
      $bus_types = BusType::all();
      $bus_operators = BusOperator::all();
      return view('sellticket',compact('bus_types','bus_operators'));
    }

    public function addTicket(Request $request){
        $date = strtotime($request->get('journey_time'));
        $source_name = $request->get('source_id');
        $destination_name = $request->get('destination_id');
        $source = City::where('name' , $source_name)->first();
        $destination = City::where('name' , $destination_name)->first();
        $data = [
          'source_id'=>$source->id,
          'destination_id'=>$destination->id,
          'journey_time'=>date('Y-m-d H:i:s', $date),
          'bus_operator_id'=>$request->get('bus_operator_id'),
          'bus_type_id'=>$request->get('bus_type_id'),
          'price'=>$request->get('price'),
          'detail'=>$request->get('detail'),
          'status'=>1
        ];
        $user = User::where('mobile',$request->get('mobile'))->first();
        if($user)
        {
          $data['user_id'] = $user->id;
          Ticket::create($data);
          $alert = 'Thanks for your posting';
        }
        else{
          $sendSms = new Message();
          $pin = $sendSms->generatePswd(8);
          $message = 'Welcome to TicketMeMe, You just registered with us. Your password is : '.$pin.' You can change your password anytime from your Dashboard, Thanks-Team TicketMeMe';
          // Sending Password
          $sendSms->sendsms($request->get('mobile'), $message);
          $user = User::create([
            'name'=>$request->get('name'),
            'mobile'=>$request->get('mobile'),
            'password' => bcrypt($pin),
          ]);
          $data['user_id'] = $user->id;
          Ticket::create($data);
          $alert = 'You have newly registered on our website';
        }
        return redirect('/')->with(['message'=>'ticketpost','text'=>$alert]);
    }
    public  function subscribe(Request $request)
    {
      $phone = $request->get('phone');
      $sendSms = new Message();
      $message = 'Thanks for your subscription!, Thanks-Team TicketMeMe';
        // Sending Password
      $sendSms->sendsms($phone, $message);
      Subscribe::create(['mobile'=>$phone]);
      return redirect('/')->with('message','subscribe');
    }
    public function feedback(Request $request)
    {
      $data = $request->all();
      $sendSms = new Message();
      $message = 'Thanks for your feedback!, Thanks-Team TicketMeMe';
      $sendSms->sendsms($data['mobile'], $message);
      Feedback::create($data);
      return redirect('/')->with('message','feedback');
    }
    public function requestTicket(Request $request)
    {
      $message = $request->get('message');
      $mobile = $request->get('mobile');
      $ticket = $request->get('ticket_id');
      $ticket = Ticket::find($ticket);
      $sendSms = new Message();
      $str = 'Hi! Somebody request your ticket. Phone Number is '.$mobile.' He or She leave message as follow: '.$message;
      $sendSms->sendsms($data['mobile'], $str);
      $user = User::where('mobile',$mobile)->get();
      $text = 'Thanks for your ticket! Your message successfully sent to client';
      if(!count($user)){
        $sendSms = new Message();
        $pin = $sendSms->generatePswd(8);
        $message = 'Welcome to TicketMeMe, You just registered with us. Your password is : '.$pin.' You can change your password anytime from your Dashboard, Thanks-Team TicketMeMe';
        $sendSms->sendsms($mobile, $message);
        $user = User::create([
            'name' => $pin,
            'mobile' => $mobile,
            'email' => $pin.'@email.com',
            'password' => bcrypt($pin),
        ]);
        // Sending Password
        $text = 'Thanks for your ticket! Your message successfully sent to client and you have newly registered in our website';
      }
      else
        $user = $user[0];
      TicketRequest::create(['ticket_id'=>$request->get('ticket_id'),'user_id'=>$user->id,'message'=>$request->get('message')]);
      return redirect('/')->with(['message'=>'requestTicket','text'=>$text]);
    }
}

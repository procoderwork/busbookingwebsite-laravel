<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request , [
            'name' => 'required|string|max:30',
            'email' => 'string|unique:users|email|min:5|max:40|nullable'
        ]);
        $user = Auth::user();
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->save();
        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request , [
            'password' => 'required|confirmed|string|min:6'
        ]);
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back();
    }
    // public function users()
    // {
    //     $users = User::all();
    //     return view('users', ['users' => $users]);
    // }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\User;
use Message\Sms\Message;
use Illuminate\Http\Request;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetUI()
    {
        return view('auth.passwords.email');
    }

    public function sendResetedPassword(Request $request)
    {
        $this->validateMobileNumber($request);

        $sendSms = new Message();
        $pin = $sendSms->generatePswd(8);

        $message = 'Welcome to TicketMeMe, Your password just changed. Your password is : '.$pin.' Thanks-Team TicketMeMe';

        $user = User::where('mobile',$request->get('mobile'))->first();

        $user->password = bcrypt($pin);
        $user->save();
        $sendSms->sendsms($request->get('mobile'), $message);

        return redirect()->route('login');
    }

    protected function validateMobileNumber(Request $request)
    {
        $this->validate($request, ['mobile' => 'required|exists:users,mobile']);
    }
}

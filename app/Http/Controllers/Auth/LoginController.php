<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Socialite;
use App\SocialProvider;
use App\User;
use Message\Sms\Message;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try{
            $socialUser = Socialite::driver($provider)->user();
        }
        catch(\Exception $e)
        {
            return redirect('/');
        }

        // check if provider is already logged
        $socialProvider = SocialProvider::where('provider_id', $socialUser->getId())->first();
        $new_registered = false;
        if(!$socialProvider)
        {
            //create a new user and provider
            if(!count(User::where('email',$socialUser->getEmail())->get())){
                $str = "";
                $characters = array_merge(range('0','9'));
                $max = count($characters) - 1;
                for ($i = 0; $i < 10; $i++) {
                    $rand = mt_rand(0, $max);
                    $str .= $characters[$rand];
                }
                $user = User::firstOrCreate([
                    'email' => $socialUser->getEmail() ,
                    'name' => $socialUser->getName() ,
                    'mobile' => $str,
                    ]
                );
                $new_registered = true;
            }
            else{
                $user = User::where('email',$socialUser->getEmail())->first();
            }


            $user->socialProvider()->create(

                ['provider_id' => $socialUser->getId(), 'provider' => $provider]

            );
        }
        else
            $user = $socialProvider->user;
        if($user->is_enabled)
          auth()->login($user);
        if($new_registered)
            return redirect('/home')->with('message','resetMobile');
        else
            return redirect('/');

    }

}

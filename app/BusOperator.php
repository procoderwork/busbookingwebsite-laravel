<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusOperator extends Model
{
    //
    protected $guarded = ['id'];
}

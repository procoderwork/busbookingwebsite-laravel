<?php

namespace Message\Sms;

class Message
{
	public function sendsms($mobilenumber, $message){

		$senderId = "TKTMEM";
		$route = 4;
		$campaign = "TicketMeMe Auth";
		$message = $message;
		$sms = array(
		    'message' => $message,
		    'to' => array($mobilenumber)
		);
		//Prepare you post parameters
		$postData = array(
		    'sender' => $senderId,
		    'campaign' => $campaign,
		    'route' => $route,
		    'sms' => array($sms)
		);
		$postDataJson = json_encode($postData);
		$url="https://control.msg91.com/api/v2/sendsms";
		$curl = curl_init();
		curl_setopt_array($curl, array(
		    CURLOPT_URL => "$url",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => $postDataJson,
		    CURLOPT_HTTPHEADER => array(
		        "authkey: 196350AyHl6dF7y5a978c25",
		        "content-type: application/json"
		    ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);

		return $response. $err;
	}

	/*Password Generator*/
	public function generatePswd($pswdlength){
		// Generate set of alpha characters
	    $alpha = array();
	    for ($u = 65; $u <= 90; $u++) {
	        // Uppercase Char
	        array_push($alpha, chr($u));
	    }

	    // Just in case you need lower case
	    // for ($l = 97; $l <= 122; $l++) {
	    //    // Lowercase Char
	    //    array_push($alpha, chr($l));
	    // }

	    // Get random alpha character
	    $rand_alpha_key = array_rand($alpha);
	    $rand_alpha = $alpha[$rand_alpha_key];

	    // Add the other missing integers
	    $rand = array($rand_alpha);
	    for ($c = 0; $c < $pswdlength - 1; $c++) {
	        array_push($rand, mt_rand(0, 9));
	        shuffle($rand);
	    }

	    $pin = implode('', $rand);

		return $pin;
	}
}

